#include "Player.h"

// Functions Private ==============================================================
void Player::init_variables()
{
	this->movement_speed = 3.f;
	this->attack_cooldown_max = 30.f;
	this->attack_cooldown = this->attack_cooldown_max;

	this->hp_max = 100;
	this->hp = this->hp_max;
}

void Player::init_texture()
{
	// Load texture from file
	if (!this->texture.loadFromFile("textures/ship.png"))
	{
		std::cout << "!! ERROR::PLAYER::INIT_TEXTURE::Could not load texture file from 'textures/ship.png'." << std::endl;
	}
}

void Player::init_sprite()
{
	// Set texture to the sprite
	this->sprite.setTexture(this->texture);

	// Resize the sprite
	this->sprite.setScale(0.1f, 0.1f);
}

// Constructor & Destructor =======================================================
Player::Player()
{
	this->init_variables();
	this->init_texture();
	this->init_sprite();
}

Player::~Player()
{
}

// Accessors ======================================================================
const sf::Vector2f& Player::get_position() const
{
	return this->sprite.getPosition();
}

const sf::FloatRect Player::get_bounds() const
{
	return this->sprite.getGlobalBounds();
}

const int& Player::get_hp() const
{
	return this->hp;
}

const int& Player::get_hp_max() const
{
	return this->hp_max;
}

// Modifiers ======================================================================
void Player::set_position(const sf::Vector2f pos)
{
	this->sprite.setPosition(pos.x, pos.y);
}

void Player::set_position(const float pos_x, const float pos_y)
{
	this->sprite.setPosition(pos_x, pos_y);
}

void Player::set_hp(const int hp)
{
	this->hp = hp;
}

void Player::lose_hp(const int value)
{
	this->hp -= value;
	if (this->hp < 0)
	{
		this->hp = 0;
	}
}

// Functions Public ===============================================================
void Player::move(const float dir_x, const float dir_y)
{
	this->sprite.move(
		this->movement_speed * dir_x, 
		this->movement_speed * dir_y
	);
}

const bool Player::can_attack()
{
	if (this->attack_cooldown >= this->attack_cooldown_max) 
	{
		this->attack_cooldown = 0.5f;
		return true;
	}
	return false;
	
}

void Player::update_attack()
{
	if (this->attack_cooldown < this->attack_cooldown_max)
	{
		this->attack_cooldown += 1.f;
	}
}

void Player::update()
{
	this->update_attack();
}

void Player::render(sf::RenderTarget& target)
{
	target.draw(this->sprite);
}
