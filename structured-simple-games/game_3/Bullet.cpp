#include "Bullet.h"

// Constructor & Destructor ================================================

Bullet::Bullet(sf::Texture* texture, float pos_x, float pos_y, float dir_x, float dir_y, float movement_speed)
{
	this->shape.setTexture(*texture);
	this->shape.setPosition(pos_x, pos_y);
	this->direction.x = dir_x;
	this->direction.y = dir_y;
	this->movement_speed = movement_speed;
}

Bullet::~Bullet()
{
}

// Accessors
const sf::FloatRect Bullet::get_bounds() const
{
	return this->shape.getGlobalBounds();
}

// Functions public
void Bullet::update()
{
	// Movement
	this->shape.move(this->movement_speed * this->direction);
}

void Bullet::render(sf::RenderTarget* target)
{
	target->draw(this->shape);
}
