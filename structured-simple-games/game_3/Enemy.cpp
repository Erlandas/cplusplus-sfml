#include "Enemy.h"

// Funciots private ===============================================================

void Enemy::init_variables()
{
	this->point_count = rand() % 8 + 3; // min = 3 max = 10
	this->type		= 0;
	this->speed		= static_cast<float>(this->point_count) / 5;
	this->hp_max	= static_cast<int>(this->point_count);
	this->hp		= this->hp_max;
	this->damage	= this->point_count;
	this->points	= this->point_count;
}

void Enemy::init_shape()
{
	this->shape.setRadius(this->point_count * 4);
	this->shape.setPointCount(this->point_count);
	this->shape.setFillColor(sf::Color(
		rand() % 255 +1,	// RED	
		rand() % 255 + 1,	// GREEN
		rand() % 255 + 1,	// BLUE
		255					// OPACITY
	));
}

// Constructor & Destructor =======================================================
Enemy::Enemy(float pos_x, float pos_y)
{
	this->init_variables();
	this->init_shape();
	this->shape.setPosition(pos_x, pos_y);
}

Enemy::~Enemy()
{

}

// Accessors
const sf::FloatRect Enemy::get_bounds() const
{
	return this->shape.getGlobalBounds();
}

const int& Enemy::get_points() const
{
	return this->points;
}

const int& Enemy::get_damage() const
{
	return this->damage;
}

// Functions Public ==============================================================
void Enemy::update()
{
	this->shape.move(0.f, this->speed);
}

void Enemy::render(sf::RenderTarget* target)
{
	target->draw(this->shape);
}
