#pragma once

#include <map>
#include <vector>
#include <string>
#include <sstream>
#include "Player.h"
#include "Bullet.h"
#include "Enemy.h"

class Game
{
private:
	// Variables
	//	--	Window
	sf::RenderWindow* window;

	//	--	Resources
	std::map<std::string, sf::Texture*> textures;
	std::vector<Bullet*> bullets;

	//	--	GUI
	sf::Font font;
	sf::Text point_text;
	sf::Text game_over_text;

	//	--	World background
	sf::Texture world_background_texture;
	sf::Sprite world_background;

	//	--	Systems
	unsigned points;

	//	--	Player
	Player* player;

	//	--	Player GUI
	sf::RectangleShape player_hp_bar;
	sf::RectangleShape player_hp_bar_back;

	//	--	Enemies
	float spawn_timer;
	float spawn_timer_max;
	std::vector<Enemy*> enemies;

	// Functions Private
	void init_window();
	void init_textures();
	void init_GUI();
	void init_world();
	void init_systems();
	void init_player();
	void init_enemies();
public:
	// Constructors & Destructors
	Game();
	virtual ~Game();

	// Functions Public
	void run();

	void update_pool_events();
	void update_input();
	void update_GUI();
	void update_world();
	void update_collision();
	void update_bullets();
	void update_enemies();
	void update_combat();
	void update();

	void render_GUI();
	void render_world();
	void render();
};

