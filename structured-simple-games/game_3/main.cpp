/*
*	GAME 3: Swaglords of Space
*	What is covered?:
* 
*	1.	Game class
*	2.	Polygon shapes
*	3.	Textures
*	4.	Fonts
*	5.	Points
*	6.	Timer
*	7.	Healthbar
*	8.	Restart button
*	9.	Background
*/

#include "Game.h"

#include <iostream>
#include <time.h>


int main() {

	srand(time(NULL));

	Game game;

	game.run();

	return 0;
}