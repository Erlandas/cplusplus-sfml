#include "Game.h"

// Functions Private ===============================================================
void Game::init_window()
{
	this->window = new sf::RenderWindow(
		sf::VideoMode(800, 900), 
		"Swaglords of Space - Game 3", 
		sf::Style::Close | sf::Style::Titlebar
	);
	this->window->setFramerateLimit(144);
	this->window->setVerticalSyncEnabled(false);
}

void Game::init_textures()
{
	this->textures["BULLET"] = new sf::Texture();
	this->textures["BULLET"]->loadFromFile("textures/bullet.png");
}

void Game::init_GUI()
{
	// Load fonts
	if (!this->font.loadFromFile("fonts/Arcade.ttf"))
	{
		std::cout << "!! ERROR::GAME::INIT_GUI::Could not load font from 'fonts/Arcade.ttf'." << std::endl;
	}
	// Init point text
	this->point_text.setPosition(600.f, 20.f);
	this->point_text.setFont(this->font);
	this->point_text.setCharacterSize(30);
	this->point_text.setFillColor(sf::Color::White);
	this->point_text.setString("TEST");

	// Init game over text
	this->game_over_text.setFont(this->font);
	this->game_over_text.setCharacterSize(60);
	this->game_over_text.setFillColor(sf::Color::Red);
	this->game_over_text.setString("!! GAME OVER !!");
	this->game_over_text.setPosition(sf::Vector2f(
		this->window->getSize().x / 2.f - this->game_over_text.getGlobalBounds().width / 2.f ,
		this->window->getSize().y / 2.f - this->game_over_text.getGlobalBounds().height / 2.f
	));

	// Init player GUI
	this->player_hp_bar.setSize(sf::Vector2f(300.f, 25.f));
	this->player_hp_bar.setFillColor(sf::Color(100,100,100,150));
	this->player_hp_bar.setPosition(sf::Vector2f(20.f, 20.f));

	this->player_hp_bar_back = this->player_hp_bar;
	this->player_hp_bar_back.setFillColor(sf::Color(25, 25, 25, 200));
}

void Game::init_world()
{
	if (!this->world_background_texture.loadFromFile("textures/background1.jpg"))
	{
		std::cout << "!! ERROR::GAME::INIT_WORLD::Could not load texture from 'textures/background1.jpg'." << std::endl;
	}
	this->world_background.setTexture(this->world_background_texture);
}

void Game::init_systems()
{
	this->points = 0;
}

void Game::init_player()
{
	this->player = new Player();
}

void Game::init_enemies()
{
	this->spawn_timer_max = 50.f;
	this->spawn_timer = this->spawn_timer_max;
}

// Constructors & Destructors ======================================================
Game::Game()
{
	this->init_window();
	this->init_textures();
	this->init_GUI();
	this->init_world();
	this->init_systems();
	this->init_player();
	this->init_enemies();
}

Game::~Game()
{
	delete this->window;
	delete this->player;

	// Delete textures
	//	--	Example of deleting map pointers
	for (auto& texture : this->textures)
	{
		delete texture.second;
	}

	// Delete bullets
	for (auto *i : this->bullets)
	{
		delete i;
	}

	// Delete enemies
	for (auto* i : this->enemies)
	{
		delete i;
	}

}

// Functions Public ================================================================
void Game::run()
{
	while (this->window->isOpen())
	{
		this->update_pool_events();
		if (this->player->get_hp() > 0)
		{
		this->update();
		}
		this->render();
	}
	
}

void Game::update_pool_events()
{
	sf::Event e;
	while (this->window->pollEvent(e))
	{
		
		if (e.type == sf::Event::Closed)
		{
			this->window->close();
		}
		if (e.type == sf::Event::KeyPressed
			&& e.key.code == sf::Keyboard::Escape)
		{
			this->window->close();
		}
	}
}

void Game::update_input()
{
	// Move Player
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::A))
	{
		this->player->move(-1.f, 0.f);
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D))
	{
		this->player->move(1.f, 0.f);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::W))
	{
		this->player->move(0.f, -1.f);
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S))
	{
		this->player->move(0.f, 1.f);
	}

	if (sf::Mouse::isButtonPressed(sf::Mouse::Left)
		&& this->player->can_attack())
	{
		this->bullets.push_back(new Bullet(
			this->textures["BULLET"],
			this->player->get_position().x + this->player->get_bounds().width / 2.f,
			this->player->get_position().y,
			0.f,
			-1.f,
			5.f
		));
	}
}

void Game::update_GUI()
{
	std::stringstream ss;
	ss << "Points: " << this->points << std::endl;
	this->point_text.setString(ss.str());

	// Update player GUI
	float hp_percent = static_cast<float>(this->player->get_hp()) / this->player->get_hp_max();
	this->player_hp_bar.setSize(sf::Vector2f(
		300.f * hp_percent, 
		this->player_hp_bar.getSize().y
	));

}

void Game::update_world()
{

}

void Game::update_collision()
{
	// Left world collision
	if (this->player->get_bounds().left < 0.f)
	{
		this->player->set_position(
			0.f, 
			this->player->get_bounds().top
		);
	}
	// Right world collision
	else if (this->player->get_bounds().left + this->player->get_bounds().width >= this->window->getSize().x)
	{
		this->player->set_position(
			this->window->getSize().x - this->player->get_bounds().width,
			this->player->get_bounds().top
		);
	}
	// Top world collision
	if (this->player->get_bounds().top < 0.f)
	{
		this->player->set_position(
			this->player->get_bounds().left, 
			0.f
		);
	}
	
	// Bottom world collision
	else if (this->player->get_bounds().top + this->player->get_bounds().height >= this->window->getSize().y)
	{
		this->player->set_position(
			this->player->get_bounds().left,
			this->window->getSize().y - this->player->get_bounds().height
		);
	}
}

void Game::update_bullets()
{
	unsigned counter = 0;
	for (auto* bullet : this->bullets)
	{
		bullet->update();

		// Bullet culling (top of screen)
		if (bullet->get_bounds().top + bullet->get_bounds().height < 0.f)
		{
			// Delete bullet
			delete this->bullets.at(counter);
			this->bullets.erase(bullets.begin() + counter);
		}

		++counter;
	}
}

void Game::update_enemies()
{
	// Spawning
	this->spawn_timer += 0.5f;
	if (this->spawn_timer >= this->spawn_timer_max)
	{
		this->enemies.push_back(new Enemy(
			rand() % this->window->getSize().x - 20.f ,
			-100.f
		));
		this->spawn_timer = 0.f;
	}

	// Update
	unsigned counter = 0;
	for (auto* enemy : this->enemies)
	{
		enemy->update();

		// Enemy culling (bottom of screen)
		if (enemy->get_bounds().top > this->window->getSize().y)
		{
			// Delete enemy if reaches bottom
			delete this->enemies.at(counter);
			this->enemies.erase(this->enemies.begin() + counter);
		}

		// enemy player collision
		else if (enemy->get_bounds().intersects(this->player->get_bounds()))
		{
			// Delete enemy if touches player
			this->player->lose_hp(this->enemies.at(counter)->get_damage());
			delete this->enemies.at(counter);
			this->enemies.erase(this->enemies.begin() + counter);
		}

		++counter;
	}
	
}

void Game::update_combat()
{
	for (int i = 0; i < this->enemies.size(); i++)
	{
		bool enemy_deleted = false;
		for (size_t k = 0; k < this->bullets.size() && enemy_deleted == false; k++)
		{
			if (this->enemies.at(i)->get_bounds().intersects(this->bullets.at(k)->get_bounds()))
			{
				this->points += this->enemies.at(i)->get_points();

				delete this->enemies.at(i);
				this->enemies.erase(this->enemies.begin() + i);

				delete this->bullets.at(k);
				this->bullets.erase(this->bullets.begin() + k);

				enemy_deleted = true;
			}
		}
	}
}

void Game::update()
{
	this->update_input();
	this->player->update();
	this->update_collision();
	this->update_bullets();
	this->update_enemies();
	this->update_combat();
	this->update_GUI();
	this->update_world();
}

void Game::render_GUI()
{
	this->window->draw(this->point_text);
	this->window->draw(this->player_hp_bar_back);
	this->window->draw(this->player_hp_bar);
}

void Game::render_world()
{
	this->window->draw(this->world_background);

}

void Game::render()
{
	this->window->clear();

	// Draw world stuff
	this->render_world();

	// Draw all the stuff
	this->player->render(*this->window);

	for (auto* bullet : this->bullets)
	{
		bullet->render(this->window);
	}

	for (auto* enemy : this->enemies)
	{
		enemy->render(this->window);
	}

	// Draw GUI stuff
	this->render_GUI();

	// Game over screen
	if (this->player->get_hp() <= 0)
	{
		this->window->draw(this->game_over_text);
	}

	this->window->display();
}
