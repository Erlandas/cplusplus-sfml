#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <iostream>

class Player
{
private:
	// Variables
	sf::Texture texture;
	sf::Sprite sprite;
	float movement_speed;
	float attack_cooldown;
	float attack_cooldown_max;
	int hp;
	int hp_max;

	// Functions Private
	void init_variables();
	void init_texture();
	void init_sprite();

public:
	// Constructor & Destructor
	Player();
	virtual ~Player();

	// Accessors
	const sf::Vector2f& get_position() const;
	const sf::FloatRect get_bounds() const;
	const int& get_hp() const;
	const int& get_hp_max() const;

	// Modifiers
	void set_position(const sf::Vector2f pos);
	void set_position(const float pos_x, const float pos_y);
	void set_hp(const int hp);
	void lose_hp(const int value);

	// Functions Public
	void move(const float dir_x, const float dir_y);
	const bool can_attack();
	void update_attack();
	void update();
	void render(sf::RenderTarget& target);

};

