#pragma once

#include <SFML/Graphics.hpp>

class Enemy
{
private:
	// Variables
	unsigned point_count;
	sf::CircleShape shape;
	int type;
	float speed;
	int hp;
	int hp_max;
	int damage;
	int points;

	// Funciots private
	void init_variables();
	void init_shape();

public:
	// Constructor & Destructor
	Enemy(float pos_x, float pos_y);
	virtual ~Enemy();

	// Accessors
	const sf::FloatRect get_bounds() const;
	const int& get_points() const;
	const int& get_damage() const;

	// Functions Public
	void update();
	void render(sf::RenderTarget* target);


};

