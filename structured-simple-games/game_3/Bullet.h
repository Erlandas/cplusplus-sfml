#pragma once

#include <SFML/Graphics.hpp>
#include <iostream>

class Bullet
{
private:
	// Variables
	sf::Sprite shape;
	
	sf::Vector2f direction;
	float movement_speed;

public:
	// Constructor & Destructor
	Bullet() = default;
	Bullet(sf::Texture* texture, float pos_x, float pos_y, float dir_x, float dir_y, float movement_speed);
	virtual ~Bullet();

	// Accessors
	const sf::FloatRect get_bounds() const;

	// Functions public
	void update();
	void render(sf::RenderTarget* target);
};

