/*
*	GAME 2 NAME:
		'Pick Up Swag Balls'

*	GAME 2 what is covered:
* 
*	1.	Create window
*	2.	Game Class
*	3.	Moving shapes
*	4.	Fonts
*	5.	Spawing balls (new shape type)
*	6.	Player and Balls classes
*	7.	Moving player using keyboard
*	8.	Fonts and text
*	9.	Simple collision
*	10.	Health bar
*/

#include "Game.h"


int main() {

	//Initialize random seed
	srand(static_cast<unsigned>(time(NULL)));

	//Initialize game object
	Game game;

	//Game loop
	while (game.running()) {
		game.update();

		game.render();
	}


	// End of application
	return 0;
}