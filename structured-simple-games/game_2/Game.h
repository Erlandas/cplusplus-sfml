#pragma once

#include <iostream>
#include <ctime>
#include <vector>
#include <sstream>


#include "Player.h"
#include "SwagBall.h"

class Game
{
private:
	//Variables
	sf::VideoMode video_mode;
	sf::RenderWindow* window;
	bool game_over;
	sf::Event window_event;

	Player player;

	int points;

	sf::Font font;
	sf::Text gui_text;
	sf::Text end_game_text;

	std::vector<SwagBall> swag_balls;
	float spawn_timer_max;
	float spawn_timer;
	int max_swag_balls;

	void init_variables();
	void init_window();
	void init_fonts();
	void init_text();

public:

	//Constructor & Destructor
	Game();
	~Game();

	//Accessors
	const bool& get_game_over() const;

	//Modifiers

	//Functions
	const bool running() const;
	void poll_events();

	void spawn_swag_balls();
	void update_player();
	void update_collision();
	const int randomize_ball_type() const;
	void update_gui();
	void update();
	void render_gui(sf::RenderTarget* target);
	void render();
};

