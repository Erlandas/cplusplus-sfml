#include "Game.h"

void Game::init_variables()
{
	this->game_over = false;
	this->spawn_timer_max = 10.f;
	this->spawn_timer = this->spawn_timer_max;
	this->max_swag_balls = 10;
	this->points = 0;

}

void Game::init_window()
{
	this->video_mode = sf::VideoMode(800, 600);
	this->window = new sf::RenderWindow(video_mode, "Swag Ballssss....", sf::Style::Close | sf::Style::Titlebar);
	this->window->setFramerateLimit(60);
}

void Game::init_fonts()
{
	if (!this->font.loadFromFile("fonts/Arcade.ttf")) 
	{
		std::cout << "! ERROR::GAME::INIT_FONTS::Could not import font frm 'fonts/Arcade.ttf'." << std::endl;
	} 
	else
	{
		std::cout << "GAME::INIT_FONTS::Font loaded succesfully." << std::endl;
	}
}

void Game::init_text()
{
	// gui text
	this->gui_text.setFont(this->font);
	this->gui_text.setFillColor(sf::Color::Red);
	this->gui_text.setCharacterSize(32);

	// end game text
	this->end_game_text.setFont(this->font);
	this->end_game_text.setFillColor(sf::Color::Red);
	this->end_game_text.setCharacterSize(50);
	this->end_game_text.setPosition(sf::Vector2f(10.f, 300.f));
	this->end_game_text.setString("GAME OVER");

}


// Constructors & Destructors
Game::Game()
{
	this->init_variables();
	this->init_window();
	this->init_fonts();
	this->init_text();
}

Game::~Game()
{
	delete this->window;
}

const bool& Game::get_game_over() const
{
	return this->game_over;
}


// Functions
const bool Game::running() const
{
	return this->window->isOpen();
}

void Game::poll_events()
{
	while (this->window->pollEvent(this->window_event)) 
	{
		switch (this->window_event.type) 
		{
			case sf::Event::Closed: 
			{
				this->window->close();
				break;
			}
			case sf::Event::KeyPressed:
			{
				if (this->window_event.key.code == sf::Keyboard::Escape)
				{
					this->window->close();
				}
				break;
			}
		}
	}
}

void Game::spawn_swag_balls()
{
	// timer
	if (this->spawn_timer < this->spawn_timer_max) 
	{
		this->spawn_timer += 1.f;
	}
	else
	{
		if (this->swag_balls.size() < this->max_swag_balls)
		{
			this->swag_balls.push_back(SwagBall(*this->window, this->randomize_ball_type()));
			this->spawn_timer = 0.f;
		}
	}
}

void Game::update_player()
{
	this->player.update(this->window);

	if (this->player.get_hp() <= 0) {
		this->game_over = true;
	}
}

void Game::update_collision()
{
	// Check the collision
	for (size_t i = 0; i < swag_balls.size(); i++)
	{
		if (this->player.get_shape().getGlobalBounds().intersects(swag_balls[i].get_shape().getGlobalBounds()))
		{
			switch (swag_balls[i].get_type()) {
				case Swag_Ball_Types::DEFAULT:
				{
					this->points++;
					break;
				}
				case Swag_Ball_Types::DAMAGING:
				{
					this->player.take_damage(1);
					break;
				}
				case Swag_Ball_Types::HEALING:
				{
					this->player.gain_health(1);
					break;
				}
			}			

			// Remove ball
			this->swag_balls.erase(this->swag_balls.begin() + i);
		}
	}

}

const int Game::randomize_ball_type() const
{

	int type = Swag_Ball_Types::DEFAULT;

	int rand_value = rand() % 100 + 1;
	if (rand_value > 60 && rand_value <= 80)
	{
		type = Swag_Ball_Types::DAMAGING;
	}
	else if (rand_value > 80 && rand_value <= 100)
	{
		type = Swag_Ball_Types::HEALING;
	}
	


	return type;
}

void Game::update_gui()
{
	std::stringstream ss;

	ss	<< " - Points: " << this->points << std::endl
		<< " - Health: " << this->player.get_hp() << " / " << this->player.get_hp_max() << std::endl;

	this->gui_text.setString(ss.str());
}

void Game::update()
{
	this->poll_events();
	if (this->game_over == false)
	{
		this->spawn_swag_balls();
		this->update_player();
		this->update_collision();
		this->update_gui();
	} 

}

void Game::render_gui(sf::RenderTarget* target)
{
	target->draw(this->gui_text);
}

void Game::render()
{
	this->window->clear();
	
	this->player.render(this->window);

	for (auto i : this->swag_balls)
	{
		i.render(*this->window);
	}

	// GUI elements
	this->render_gui(this->window);

	// Render end text
	if (this->game_over == true)
	{
		this->window->draw(this->end_game_text);
	}

	this->window->display();
}

