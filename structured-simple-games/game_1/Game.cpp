#include "Game.h"

// Private Functions
void Game::init_variables()
{
	this->window = nullptr;

	// Game logic
	this->game_over = false;
	this->points = 0;
	this->health = 10;
	this->enemy_spawn_timer_max = 100.f;
	this->enemy_spawn_timer = this->enemy_spawn_timer_max;
	this->max_enemies = 10;
	this->mouse_held = false;

}

void Game::init_window()
{
	this->video_mode.width = 800;
	this->video_mode.height = 600;

	this->window = new sf::RenderWindow(video_mode, "GAME 1", sf::Style::Titlebar | sf::Style::Close);
	this->window->setFramerateLimit(60);
}

void Game::init_fonts()
{
	if (!this->font.loadFromFile("fonts/Arcade.ttf")) {
		std::cout << "ERROR::GAME::INIT_FONTS::Failed to load font!" << std::endl;
	}
	else {
		std::cout << "Font loaded!" << std::endl;
	}
}

void Game::init_text()
{
	this->UI_text.setFont(this->font);
	this->UI_text.setCharacterSize(30);
	this->UI_text.setFillColor(sf::Color::White);
	this->UI_text.setString("NONE");
}

void Game::init_enemies()
{
	this->enemy.setPosition(sf::Vector2f(10.f, 10.f));
	this->enemy.setSize(sf::Vector2f(100.f, 100.f));
	this->enemy.setFillColor(sf::Color::Cyan);
	//this->enemy.setOutlineColor(sf::Color::Green);
	//this->enemy.setOutlineThickness(3.f);


}

// Constructors & Destructors
Game::Game()
{
	this->init_variables();
	this->init_window();
	this->init_fonts();
	this->init_text();
	this->init_enemies();
	
}

Game::~Game()
{
	// prevent memory leaks delete window pointer
	delete this->window;
}

// Accessors
const bool Game::running() const
{
	return this->window->isOpen();
}

const bool Game::get_game_over() const
{
	return this->game_over;
}



// Functions
void Game::spawn_enemy()
{
	/*
	*	@return void
	* 
	*	Spawns enemies and sets their types, colors and positions randomly
	*	-	sets random position
	*	-	sets random color
	*	-	adds enemy to the vector
	*/

	this->enemy.setPosition(
		static_cast<float>(rand() % static_cast<int>(this->window->getSize().x - this->enemy.getSize().x)),
		0.f
	);

	// Randomize enemy type
	int type = rand() % 5;

	switch (type) {
		case 0: {
			this->enemy.setSize(sf::Vector2f(20.f, 20.f));
			this->enemy.setFillColor(sf::Color::Magenta);
			break;
		}
		case 1: {
			this->enemy.setSize(sf::Vector2f(30.f, 30.f));
			this->enemy.setFillColor(sf::Color::Blue);
			break;
		}
		case 2: {
			this->enemy.setSize(sf::Vector2f(50.f, 50.f));
			this->enemy.setFillColor(sf::Color::Cyan);
			break;
		}
		case 3: {
			this->enemy.setSize(sf::Vector2f(70.f, 70.f));
			this->enemy.setFillColor(sf::Color::Red);
			break;
		}
		case 4: {
			this->enemy.setSize(sf::Vector2f(100.f, 100.f));
			this->enemy.setFillColor(sf::Color::Green);
			break;
		}
		default: {
			this->enemy.setSize(sf::Vector2f(100.f, 100.f));
			this->enemy.setFillColor(sf::Color::Yellow);
			break;
		}
			
	}

	

	// Spawn enemy
	this->enemies.push_back(this->enemy);

	// Remove enemies at end of screen
}

void Game::poll_events()
{
	while (this->window->pollEvent(this->ev)) {
		switch (this->ev.type) {
			case sf::Event::Closed: {
				this->window->close();
				break;
			}

			case sf::Event::KeyPressed: {
				if (this->ev.key.code == sf::Keyboard::Escape) {
					this->window->close();
				}
				break;
			}
		}
	}
}

void Game::update_mouse_positions()
{
	/*
	*	@return void
	*	
	*	Updates mouse positions:
	*	--	Mouse positions relative to window (Vector2i)
	*/
	this->mouse_position_window = sf::Mouse::getPosition(*this->window);
	// Taking mouse pos and mapping to pixel on a window
	this->mouse_position_view = this->window->mapPixelToCoords(this->mouse_position_window);
}

void Game::update_text()
{
	std::stringstream ss;

	ss << "Points: " << this->points 
		<< "\nHealth: " << this->health;

	this->UI_text.setString(ss.str());
}

void Game::update_enemies()
{

	/*
	*	@return void
	* 
	*	Updates enemy spawn timer and spawns enemies
	*	when the total amount of enemies is smaller than the maximum.
	*	-	moves enemies downwards.
	*	-	removes enemies at the edge of the screen
	*/

	// Updating timer for enemy spawning
	if (this->enemies.size() < this->max_enemies) {
		if (this->enemy_spawn_timer >= this->enemy_spawn_timer_max) {
			// spawn enemy and reset timer
			this->spawn_enemy();
			this->enemy_spawn_timer = 0.f;
		}
		else {
			this->enemy_spawn_timer += 1.f;
		}
	}

	// Moving and updating enemies
	for (int i = 0; i < this->enemies.size(); i++)
	{
		bool deleted = false;
		deleted = false;

		this->enemies[i].move(0.f, 3.f);

		// If the enemy out of screen bounds
		if (this->enemies[i].getPosition().y > this->window->getSize().y) {
			this->enemies.erase(this->enemies.begin() + i);
			this->health -= 1;
			std::cout << "Health: " << this->health << std::endl;
		}

	}

	// check if clicked upon KILL enemies
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
		if (this->mouse_held == false) {
			this->mouse_held = true;
			bool deleted = false;
			for (size_t i = 0; i < this->enemies.size() && deleted == false; i++)
			{
				if (this->enemies[i].getGlobalBounds().contains(this->mouse_position_view)) {

					// Gain points
					if (this->enemies[i].getFillColor() == sf::Color::Magenta) {
						this->points += 10;
					}
					else if (this->enemies[i].getFillColor() == sf::Color::Blue) {
						this->points += 7;
					}
					else if (this->enemies[i].getFillColor() == sf::Color::Cyan) {
						this->points += 5;
					}
					else if (this->enemies[i].getFillColor() == sf::Color::Red) {
						this->points += 3;
					}
					else {
						this->points += 1;
					}
					
					std::cout << "Points: " << this->points << std::endl;

					// Delete the enemy
					deleted = true;
					this->enemies.erase(this->enemies.begin() + i);
				}

			}
		}
	}
	else {
		this->mouse_held = false;
	}


}

void Game::update()
{
	this->poll_events();

	if (this->game_over == false) {
		this->update_mouse_positions();

		this->update_text();

		this->update_enemies();
	}

	// End game condition
	if (this->health <= 0) {
		this->game_over = true;
	}
	// Update mouse position
	//	--	Relative to screen
	//std::cout << "Mouse position: " << sf::Mouse::getPosition().x << " " << sf::Mouse::getPosition().y << std::endl;
	//	--	Relative to window
	//std::cout << "Mouse position: " 
	//	<< sf::Mouse::getPosition(*this->window).x << " " 
	//	<< sf::Mouse::getPosition(*this->window).y << std::endl;

}

void Game::render_text(sf::RenderTarget& target)
{
	target.draw(this->UI_text);
}

void Game::render_enemies(sf::RenderTarget& target)
{
	// rendering all the enemies
	for (auto& enemy : this->enemies)
	{
		target.draw(enemy);
	}
}

void Game::render()
{
	/*
	*	@return void
	* 
	*	-	Clear old frame
	*	-	Render objects
	*	-	Display frame in window
	* 
	*	Renders the game object
	*/
	this->window->clear();

	// draw game objects
	this->render_enemies(*this->window);

	this->render_text(*this->window);

	this->window->display();
}





