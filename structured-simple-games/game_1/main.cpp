/*
	GAME 1 what is covered:

	1.	Window
	2.	Game Class
	3.	Game loop
	4.	Update & rendering
	5.	Create shapes
	6.	Move Shapes
	7.	Positions
	8.	Getting mouse positions and input
	9.	Getting keyboard inputs
	10.	Removing shapes

*/

#include <iostream>
#include "Game.h"


int main() {

	// Init srand
	std::srand(static_cast<unsigned>(time(NULL)));

	// Init game engine
	Game game;

	// Game loop
	while (game.running() == true && game.get_game_over() == false)
	{
		// Update
		game.update();

		// render
		game.render();

	}

	// End of application
	return 0;
}