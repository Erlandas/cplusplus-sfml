#pragma once

#include <iostream>
#include <vector>
#include <ctime>
#include <sstream>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Window.hpp>
#include <SFML/Network.hpp>

/*
*	Class that acts as the game engine.
*	Wrapper class.
*/

class Game
{
private:
	// Variables
	// Window
	sf::RenderWindow* window;
	sf::VideoMode video_mode;
	sf::Event ev;

	// Mouse positions
	sf::Vector2i mouse_position_window;
	sf::Vector2f mouse_position_view;

	// Resources
	sf::Font font;

	// Text
	sf::Text UI_text;

	// Game logic
	bool game_over;
	unsigned points;
	int health;
	float enemy_spawn_timer;
	float enemy_spawn_timer_max;
	int max_enemies;
	bool mouse_held;

	// Game objects
	std::vector<sf::RectangleShape> enemies;
	sf::RectangleShape enemy;

	// Private functions
	void init_variables();
	void init_window();
	void init_fonts();
	void init_text();
	void init_enemies();

public:

	// Constructors & Destructors
	Game();
	virtual ~Game();

	// Accessors
	const bool running() const;
	const bool get_game_over() const;

	// Functions
	void spawn_enemy();

	void poll_events();

	void update_mouse_positions();
	void update_text();
	void update_enemies();
	void update();

	void render_text(sf::RenderTarget& target);
	void render_enemies(sf::RenderTarget& target);
	void render();
};

