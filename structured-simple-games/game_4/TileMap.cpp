#include "stdafx.h"
#include "TileMap.h"

// Constructors & Destructors ===============================================
TileMap::TileMap()
{
	// Initialise tilemap
}

TileMap::~TileMap()
{
}

// Functions ================================================================
void TileMap::add_tile(unsigned x, unsigned y)
{
}

void TileMap::remove_tile(unsigned x, unsigned y)
{
}

void TileMap::update()
{
}

void TileMap::render()
{
}
