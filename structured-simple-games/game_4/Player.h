#pragma once

enum PLAYER_ANIMATION_STATES
{
	IDLE = 0,
	MOVING_LEFT = 1,
	MOVING_RIGHT = 2,
	JUMPING = 3,
	FALLING = 4
};

class Player
{
private:
	// Variables
	sf::Sprite sprite;
	sf::Texture texture_sheet;

	// Animation Variables
	short animation_state;
	sf::IntRect current_frame;
	sf::Clock animation_timer;
	bool animation_switch;

	// Physics
	sf::Vector2f velocity;
	float velocity_max;
	float velocity_min;
	float acceleration;
	float drag;
	float gravity;
	float velocity_max_y;	//	For gravity 

	// Initialisation Functions
	void init_variables();
	void init_texture();
	void init_sprite();
	void init_animations();
	void init_physics();
public:
	// Constructors & Destructors
	Player();
	virtual ~Player();

	// Accessors
	const bool& get_animation_switch();
	const sf::Vector2f get_position() const;
	const sf::FloatRect get_global_bounds() const;

	// Modifiers
	void set_position(const float x, const float y);
	void reset_velocity_y();

	// PUBLIC Functions
	void reset_animation_timer();
	void move(const float dir_x, const float dir_y);
	void update_physics();
	void update_movement();
	void update_animations();
	void update();
	void render(sf::RenderTarget& target);

};

