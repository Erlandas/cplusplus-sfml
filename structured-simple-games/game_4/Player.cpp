#include "stdafx.h"
#include "Player.h"

void Player::init_variables()
{
	this->animation_state = PLAYER_ANIMATION_STATES::IDLE;
}

// Initialisation Functions ================================================================
void Player::init_texture()
{
	if (!this->texture_sheet.loadFromFile("textures/player_sheet.png"))
	{
		std::cout << "!! ERROR::PLAYER::INIT_TEXTURE::Could not load texture from 'textures/player_sheet.png'" << std::endl;
	}
}

void Player::init_sprite()
{
	this->sprite.setTexture(this->texture_sheet);
	this->current_frame = sf::IntRect(0, 0, 40, 50);
	this->sprite.setTextureRect(this->current_frame);
	this->sprite.setScale(2.5f, 2.5f);
}

void Player::init_animations()
{
	this->animation_timer.restart();
	this->animation_switch = true;
}

void Player::init_physics()
{
	this->velocity_max = 15.f;
	this->velocity_min = 1.f;
	this->acceleration = 2.f;
	this->drag = 0.87f;
	this->gravity = 4.f;
	this->velocity_max_y = 5.f;
}

// Constructors & Destructors ==============================================================
Player::Player()
{
	this->init_variables();
	this->init_texture();
	this->init_sprite();
	this->init_animations();
	this->init_physics();
}

Player::~Player()
{

}

// NOTE: get_animation_switch() & reset_animation_timer() fully restarts animation system

// Accessors ================================================================================
const bool& Player::get_animation_switch()
{
	bool anim_switch = this->animation_switch;
	if (this->animation_switch == true)
	{
		this->animation_switch = false;
	}
	return anim_switch;
}

const sf::Vector2f Player::get_position() const
{
	return this->sprite.getPosition();
}

const sf::FloatRect Player::get_global_bounds() const
{
	return this->sprite.getGlobalBounds();
}

// Modifiers ================================================================================
void Player::set_position(const float x, const float y)
{
	this->sprite.setPosition(x, y);
}

void Player::reset_velocity_y()
{
	this->velocity.y = 0.f;
}

// PUBLIC Functions =========================================================================
void Player::reset_animation_timer()
{
	this->animation_timer.restart();
	this->animation_switch = true;
}

void Player::move(const float dir_x, const float dir_y)
{
	// Acceleration
	this->velocity.x += dir_x * this->acceleration;
	

	// Limit Velocity
	if (std::abs(this->velocity.x) > this->velocity_max)
	{
		this->velocity.x = this->velocity_max * ((this->velocity.x < 0.f) ? -1.f : 1.f);
	}
}

void Player::update_physics()
{
	// Gravity
	this->velocity.y += 1.0f * this->gravity;

	// Limit gravity
	if (std::abs(this->velocity.x) > this->velocity_max_y)
	{
		this->velocity.y = this->velocity_max_y * ((this->velocity.y < 0.f) ? -1.f : 1.f);
	}

	// Deceleration
	this->velocity *= this->drag;

	// limit Deceleration
	if (std::abs(this->velocity.x) < this->velocity_min)
	{
		this->velocity.x = 0.f;
	}
	if (std::abs(this->velocity.y) < this->velocity_min)
	{
		this->velocity.y = 0.f;
	}

	// Move sprite
	this->sprite.move(this->velocity);
}

void Player::update_movement()
{
	this->animation_state = PLAYER_ANIMATION_STATES::IDLE;
	// Left movement
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		this->move(-1.f, 0.f);
		this->animation_state = PLAYER_ANIMATION_STATES::MOVING_LEFT;
	}
	// Right movement
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		this->move(1.f, 0.f);
		this->animation_state = PLAYER_ANIMATION_STATES::MOVING_RIGHT;
	}

	//// Top movement
	//if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	//{
	//	this->sprite.move(0.f, -1.f);
	//
	//}
	//// Down movement
	//else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	//{
	//	this->sprite.move(0.f, 1.f);
	//
	//}
}

void Player::update_animations()
{
	if (this->animation_state == PLAYER_ANIMATION_STATES::IDLE)
	{
		if (this->animation_timer.getElapsedTime().asSeconds() >= 0.2f || this->get_animation_switch())
		{
			this->current_frame.top = 0.f;
			this->current_frame.left += 40.f;
			if (this->current_frame.left >= 160.f) {
				this->current_frame.left = 0;
			}
	
			this->animation_timer.restart();
			this->sprite.setTextureRect(this->current_frame);
		}
	}
	else if (this->animation_state == PLAYER_ANIMATION_STATES::MOVING_RIGHT)
	{
		if (this->animation_timer.getElapsedTime().asSeconds() >= 0.1f || this->get_animation_switch())
		{
			this->current_frame.top = 50.f;
			this->current_frame.left += 40.f;
			if (this->current_frame.left >= 360.f) {
				this->current_frame.left = 0;
			}

			this->animation_timer.restart();
			this->sprite.setTextureRect(this->current_frame);
			
		}
		this->sprite.setScale(2.5f, 2.5f);
		this->sprite.setOrigin(0.f, 0.f);
	}
	else if (this->animation_state == PLAYER_ANIMATION_STATES::MOVING_LEFT)
	{
		if (this->animation_timer.getElapsedTime().asSeconds() >= 0.1f || this->get_animation_switch())
		{
			this->current_frame.top = 50.f;
			this->current_frame.left += 40.f;
			if (this->current_frame.left >= 360.f) {
				this->current_frame.left = 0;
			}

			this->animation_timer.restart();
			this->sprite.setTextureRect(this->current_frame);
			
		}
		// Divide origin by whatever we scaling with
		this->sprite.setScale(-2.5f, 2.5f);
		this->sprite.setOrigin(this->sprite.getGlobalBounds().width / 2.5f, 0.f);
	}
	else
	{
		this->animation_timer.restart();
	}
}

void Player::update()
{
	this->update_movement();
	this->update_animations();
	this->update_physics();
}

void Player::render(sf::RenderTarget& target)
{
	target.draw(this->sprite);

	// Testing circle
	sf::CircleShape circle;
	circle.setFillColor(sf::Color::Red);
	circle.setRadius(4.f);
	circle.setPosition(this->sprite.getPosition());
	target.draw(circle);
}
