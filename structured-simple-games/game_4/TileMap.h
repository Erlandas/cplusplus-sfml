#pragma once
#include "Tile.h"
class TileMap
{
private:
	// Variables
	std::vector<  std::vector<Tile*>  > tiles;

public:
	// Constructors & Destructors
	TileMap();
	~TileMap();

	// Functions
	void add_tile(unsigned x, unsigned y);
	void remove_tile(unsigned x, unsigned y);
	void update();
	void render();
};

