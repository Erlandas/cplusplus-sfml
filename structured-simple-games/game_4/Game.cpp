#include "stdafx.h"
#include "Game.h"

// Initialisation Functions ============================================================================
void Game::init_window()
{
	this->window.create(
		sf::VideoMode(1200, 600), 
		"Game 4", 
		sf::Style::Close | sf::Style::Titlebar
	);
	this->window.setFramerateLimit(60);

}

void Game::init_player()
{
	this->player = new Player();
}

// Constructors & Destructors ==========================================================================
Game::Game()
{
	this->init_window();
	this->init_player();
}

Game::~Game()
{
	delete this->player;
}

// Accessors ===========================================================================================
const sf::RenderWindow& Game::get_window()
{
	return this->window;
}

// PUBLIC Functions ====================================================================================

void Game::update_player()
{
	this->player->update();
}

void Game::update_collision()
{
	// Collision bottom of screen
	if (this->player->get_position().y + this->player->get_global_bounds().height >= this->window.getSize().y)
	{
		this->player->reset_velocity_y();
		this->player->set_position(
			this->player->get_position().x,
			this->window.getSize().y - this->player->get_global_bounds().height
		);
	}
}

void Game::update()
{
	//Pooling window events
	while (this->window.pollEvent(this->ev))
	{
		if (this->ev.type == sf::Event::Closed)
		{
			this->window.close();
		}
		else if (this->ev.type == sf::Event::KeyPressed
			&& this->ev.key.code == sf::Keyboard::Escape)
		{
			this->window.close();
		}

		if (this->ev.type == sf::Event::KeyReleased && 
			(	
					this->ev.key.code == sf::Keyboard::A
				||	this->ev.key.code == sf::Keyboard::D
				||	this->ev.key.code == sf::Keyboard::W
				||	this->ev.key.code == sf::Keyboard::S)
			)
		{
			this->player->reset_animation_timer();
		}
	}

	this->update_player();

	this->update_collision();
}

void Game::render_player()
{
	this->player->render(this->window);
}

void Game::render()
{
	this->window.clear();

	//Render game
	this->player->update();
	this->render_player();

	this->window.display();
}
