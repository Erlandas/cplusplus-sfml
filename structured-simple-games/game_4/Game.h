#pragma once
#include "Player.h"

class Game
{
private:
	// variables
	sf::RenderWindow window;
	sf::Event ev;

	// Player
	Player* player;

	// Initialisation Functions
	void init_window();
	void init_player();

public:

	// Constructors & Destructors
	Game();
	virtual ~Game();

	// Accessors
	const sf::RenderWindow& get_window();

	// PUBLIC Functions
	void update_player();
	void update_collision();
	void update();

	void render_player();
	void render();
	
};

