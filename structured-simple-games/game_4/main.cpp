/*
*	GAME 4 Simple Platformer
*	What is Included? : 
* 
*	1.	Game class
*	2.	Textured enemies
*	3.	Animations
*	4.	Fonts
*	5.	Points
*	6.	Platformer
*	7.	Leveling
*	8.	Simple menu
*	9.	Moving background
*	10.	Physics
*		--	Acceleration
*		--	Deceleration
*		--	Gravity
*	11.	Flipping animation
*/
#include "stdafx.h"
#include "Game.h"

int main()
{
	srand(static_cast<unsigned>(time(0)));

	Game game;

	while (game.get_window().isOpen())
	{
		game.update();
		game.render();

	}

	return 0;
}