#pragma once
class Tile
{
private:
	// Variables
	sf::Sprite sprite;
	const bool damaging;

public:
	// Constructors & Destructors
	Tile(sf::Texture& texture_sheet, sf::IntRect texture_rect, bool damaging = false);
	virtual ~Tile();

	// Accessors
	const sf::FloatRect get_global_bounds() const;

	// Functions
	void update();
	void render(sf::RenderTarget& target);
};

