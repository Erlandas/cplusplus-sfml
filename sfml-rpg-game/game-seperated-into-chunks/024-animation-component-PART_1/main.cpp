// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Animation Component
<<PART 1>>
Changes made:
	NOTE:	When we instantiate entities those entities wont have textures,
			When we add AnimationComponent to entity then this entity will 
			have animation

	--	AnimationComponent class created
	--	Created Animation class within AnimationComponent class
		this way we can have Animations within AnimationComponent
	--	Blueprint in both class are in .h file check those
	--	No major definitions at this moment
*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}