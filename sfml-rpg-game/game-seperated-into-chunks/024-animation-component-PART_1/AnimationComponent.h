#pragma once
class AnimationComponent
{
private:
	// Classes
	class Animation
	{
	public:
		// Variables
		sf::Texture& texture_sheet;
		float animation_speed;
		int width;
		int height;
		sf::IntRect start_rect;
		sf::IntRect end_rect;

		Animation(sf::Texture& texture_sheet, float animation_speed,
			int start_x, int start_y, int end_x, int end_y, int width, int height)
			: texture_sheet(texture_sheet), animation_speed(animation_speed), 
			width(width), height(height)
		{
			this->start_rect = sf::IntRect(start_x, start_y, this->width, this->height);
			this->end_rect = sf::IntRect(end_x, end_y, this->width, this->height);
		}

		// Functions
		void update(const float& dt) {

		}
		void pause();
		void reset();
	};

	// Variables
	std::map<std::string, Animation> animations;

	// Initialisation blocks/functions
public:
	// Constructors & Destructors
	AnimationComponent();
	virtual ~AnimationComponent();

	// Functions
};

