// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Animation Component Speed Modifier

<<PART 1>>
Changes made:
	Player::
	--	Added new animation for attack
	--	Some changes in a constructor, minor changes positioning hitbox

	AnimationComponent::
	--	Overloaded play() function created that plays animayion by desired speed

	AnimationComponent::Animation::
	--	Overloaded play() function created that plays animayion by desired speed

	MovementComponent::
	--	Added accessor for max velocity
*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}