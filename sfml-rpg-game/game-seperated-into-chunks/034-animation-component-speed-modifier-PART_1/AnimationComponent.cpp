#include "pch.h"
#include "AnimationComponent.h"

// Constructors & Destructors =================================================================
AnimationComponent::AnimationComponent(sf::Sprite& sprite, sf::Texture& texture_sheet)
	: sprite(sprite), texture_sheet(texture_sheet), last_animation(nullptr)
{

}

AnimationComponent::~AnimationComponent()
{
	for(auto & i : this->animations)
	{
		delete i.second;
	}
}
// Functions ==================================================================================

// --	Animation Related Functions ===========================================================
void AnimationComponent::add_animation(
	const std::string key, 
	float animation_timer, 
	int start_frame_x, int start_frame_y, int frames_x, int frames_y, 
	int width, int height
)
{
	this->animations[key] = new Animation(
		this->sprite, this->texture_sheet,
		animation_timer,
		start_frame_x, start_frame_y, frames_x, frames_y,
		width, height
	);
}

// --	Update Functions (play) ===============================================================
void AnimationComponent::play(const std::string key, const float& dt)
{
	// Reset animation, if not equal animation we trying to play
	if (this->last_animation != this->animations[key])
	{
		if (this->last_animation == nullptr)
		{
			this->last_animation = this->animations[key];
		}
		else
		{
			this->last_animation->reset();
			this->last_animation = this->animations[key];
		}
		
	}

	this->animations[key]->play(dt);
}

void AnimationComponent::play(
	const std::string key, 
	const float& dt, 
	const float& modifier,
	const float& modifier_max
)
{
	// Reset animation, if not equal animation we trying to play
	if (this->last_animation != this->animations[key])
	{
		if (this->last_animation == nullptr)
		{
			this->last_animation = this->animations[key];
		}
		else
		{
			this->last_animation->reset();
			this->last_animation = this->animations[key];
		}

	}

	this->animations[key]->play(dt);
}

