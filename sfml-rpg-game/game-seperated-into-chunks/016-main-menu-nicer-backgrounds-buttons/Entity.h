#pragma once
/*
* Entity class is a base for any type of entity we have
*/
class Entity
{
private:


protected:
	sf::RectangleShape shape;
	float movement_speed;

public:
	// Constructors & Destructors
	Entity();
	virtual ~Entity();

	// Functions
	virtual void move(const float& dt, const float dir_x, const float dir_y);

	virtual void update(const float& dt);
	virtual void render(sf::RenderTarget* target);
};

