// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Making Main Menu Nicer
Changes made:
	MainMenuState::
	--	created init_background()
	--	created init_variables()
	--	Extra variables added for background
	--	Debuging code added for positions in render()
	--	Added extra button for settings
	--	Positions changed for buttons
*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}