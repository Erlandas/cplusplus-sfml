// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Priority Animation
NOTE:	All changes can be observed in git by commits
<<PART 3>>
Changes made:
	Player::
	--	Optimized while setting origin and scale
		Sets only if needed
	--	House keeping update function splitted in few separate functions

<<PART 2>>
Changes made:
	--	Accessors added for status if animation is done playing

<<PART 1>>
Changes made:
	--	Minor changes in animation component class time relevant to priority animation
	--	Minor changes in player class to handle priority also as testing
	--	Priority kind of working at this stage, but not as intended
*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}