// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Entity updates

NOTE:	Making more general class that alows us to create more classes based on Entity class

Changes made:
	Entity::
	--	created 'void create_sprite(sf::Texture* texture);'
	--	variables added
*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}