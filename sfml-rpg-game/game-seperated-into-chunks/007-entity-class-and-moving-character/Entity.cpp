#include "pch.h"
#include "Entity.h"
// Constructors & Destructors =================================================================
Entity::Entity()
{
	this->shape.setSize(sf::Vector2f(50.f, 50.f));
	this->movement_speed = 100.f;
}

Entity::~Entity()
{
}

// Functions ==================================================================================
void Entity::move(const float& dt, const float dir_x, const float dir_y)
{
	this->shape.move(
		dir_x * this->movement_speed * dt,
		dir_y * this->movement_speed * dt);
}

void Entity::update(const float& dt)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		this->move(dt, -1.f, -0.f);
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		this->move(dt, 1.f, -0.f);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		this->move(dt, 0.f, -1.f);
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		this->move(dt, 0.f, 1.f);
	}
}

void Entity::render(sf::RenderTarget* target)
{
	target->draw(this->shape);
}
