// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Entity class and moving character
Changes made:
	Entity class created
	Entity class is a base for any type of entity we have

	State class:
	--	Entity included

	GameState class:
	NOTE: At this moment only testing later entity class will pure virtual
	--	Entity instanciated
	--	Entity updated in 'void update(dt)'
	--	Entity rendered in 'void render(target)'


*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}