#include "pch.h"
#include "GameState.h"

// Construcors & Destructors ==================================================================
GameState::GameState(sf::RenderWindow* window)
	: State (window)
{
}

GameState::~GameState()
{
}

// Public Functions ===========================================================================

void GameState::end_state()
{
	std::cout << "Ending game state!!\n";
}

//	--	Update Functions ----------------------------------------------------------------------
void GameState::update_key_binds(const float& dt)
{
	this->check_for_quit();
}

void GameState::update(const float& dt)
{
	this->update_key_binds(dt);
	
	this->player.update(dt);
}

//	--	Render Functions ----------------------------------------------------------------------
void GameState::render(sf::RenderTarget* target)
{
	if (target)
	{
		this->player.render(target);
	}
	else
	{
		this->player.render(this->window);
	}
	
}
