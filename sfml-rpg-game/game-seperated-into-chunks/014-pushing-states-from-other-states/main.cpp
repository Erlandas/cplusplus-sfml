// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Pushing states from other states

NOTE: We want to share our universal state stack that is in Game:: class

Changes made:
	State::
	--	added state stack, pointer to state pointers. Pointing to original 
		state stack from game class

	NOTE:
		Every state related class constructor updated to pass stack of state pointers
		Now we can access state stack from any active state

*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}