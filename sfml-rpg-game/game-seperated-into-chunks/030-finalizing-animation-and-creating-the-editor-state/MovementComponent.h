#pragma once

enum movement_states
{
	IDLE = 0,
	MOVING,
	MOVING_LEFT,
	MOVING_RIGHT,
	MOVING_UP,
	MOVING_DOWN
};

class MovementComponent
{
private:
	// Variables
	sf::Sprite& sprite;

	float max_velocity;
	float acceleration;
	float deceleration;

	sf::Vector2f velocity;
	

	// Initialisation blocks/functions
	

public:
	// Constructors & Destructors
	MovementComponent(
		sf::Sprite& sprite, 
		float max_velocity, 
		float acceleration, 
		float deceleration
	);
	virtual ~MovementComponent();

	// Accessors
	const sf::Vector2f& get_velocity() const;

	// Functions
	const bool get_state(const short unsigned state) const;


	void move(const float dir_x, const float dir_y, const float& dt);
	void update(const float& dt);
};

