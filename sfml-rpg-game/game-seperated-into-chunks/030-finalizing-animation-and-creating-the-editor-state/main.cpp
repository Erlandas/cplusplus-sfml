// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Finalizing animations and creating the editor state
Changes made:
	AnimationComponent::
	--	Added animation pointer to reset animation
	--	modifications in 'AnimationComponent::play'

	NOTE:	new class created "EditorState"
			direct copy of main menu state with changes made to match newly created class

	MainMenuState::
	--	Added button for editor state
*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}