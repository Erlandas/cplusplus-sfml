#pragma once
class State
{
private:
	// Variables
	std::vector<sf::Texture*> textures;

	// Initialisation blocks/functions
public:
	// Construcors & Destructors
	State();
	virtual ~State();

	// Public Functions

	
	//	--	Update Functions
	virtual void update() = 0;	//	Pure virtual functions Inheritance
	//	--	Render Functions
	virtual void render() = 0;	//	Pure virtual functions inheritance
};

