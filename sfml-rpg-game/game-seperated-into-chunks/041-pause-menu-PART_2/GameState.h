#pragma once
#include "State.h"
#include "PauseMenu.h"

class GameState :
    public State
{
private:
    sf::Font font;
    PauseMenu* pause_menu;
    Player* player;

    // Initialisation blocks/functions
    void init_keybinds();
    void init_fonts();
    void init_textures();
    void init_pause_menu();
    void init_players();
public:
    // Construcors & Destructors
    GameState(
        sf::RenderWindow* window, 
        std::map<std::string, 
        int>* supported_keys,
        std::stack<State*>* states
    );
    virtual ~GameState();

    // Public Functions
    
    //	--	Update Functions
    void update_input(const float& dt);
    void update_player_input(const float& dt);
    void update(const float& dt);
    //	--	Render Functions
    void render(sf::RenderTarget* target = nullptr);
};

