#pragma once

#include "Player.h";


class State
{
protected:
	// Variables
	std::stack<State*>* states;
	sf::RenderWindow* window;
	std::map<std::string, int>* supported_keys;
	std::map<std::string, int> keybinds;
	bool quit;
	bool paused;

	// Mouse related
	sf::Vector2i mouse_position_screen;
	sf::Vector2i mouse_position_window;
	sf::Vector2f mouse_position_view;

	// Resources
	std::map<std::string, sf::Texture> textures;

	// Initialisation blocks/functions
	virtual void init_keybinds() = 0;
public:
	// Construcors & Destructors
	State(
		sf::RenderWindow* window, 
		std::map<std::string, 
		int>* supported_keys, 
		std::stack<State*>* states);
	virtual ~State();

	// Public Functions
	const bool& get_quit() const;

	//virtual void end_state_update() = 0;
	void end_state();

	// Pause state related
	void pause_state();
	void un_pause_state();

	//	--	Update Functions
	virtual void update_mouse_positions();
	virtual void update_input(const float& dt) = 0;
	virtual void update(const float& dt) = 0;	//	Pure virtual functions Inheritance
	//	--	Render Functions
	virtual void render(sf::RenderTarget* target = nullptr) = 0;	//	Pure virtual functions inheritance
};

