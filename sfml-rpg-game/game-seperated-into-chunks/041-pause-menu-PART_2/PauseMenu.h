#pragma once
#include "Button.h"
class PauseMenu
{
private:
	sf::Font& font;
	sf::Text menu_text;

	sf::RectangleShape background;
	sf::RectangleShape container;

	std::map<std::string, Button*> buttons;

public:
	// Constructors & Destructors
	PauseMenu(sf::RenderWindow& window, sf::Font& font);
	virtual ~PauseMenu();

	// Functions
	void update();
	void render(sf::RenderTarget& target);
};

