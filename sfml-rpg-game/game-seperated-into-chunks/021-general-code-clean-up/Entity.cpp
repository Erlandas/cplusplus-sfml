#include "pch.h"
#include "Entity.h"

// Initialisation blocks/functions ============================================================
void Entity::init_variables()
{
	this->movement_component = nullptr;
}
// Constructors & Destructors =================================================================
Entity::Entity()
{
	this->init_variables();
	
}

Entity::~Entity()
{

}

// Component Functions ========================================================================
void Entity::set_texture(sf::Texture& texture)
{
	this->sprite.setTexture(texture);
	
}

void Entity::create_movement_component(const float max_velocity)
{
	this->movement_component = new MovementComponent(this->sprite, max_velocity);
}

// Functions ==================================================================================
void Entity::set_position(const float x, const float y)
{

	this->sprite.setPosition(x, y);

}

void Entity::move(const float& dt, const float dir_x, const float dir_y)
{
	if (this->movement_component)
	{
		this->movement_component->move(dir_x, dir_y, dt); // Sets velocity
	}
	
}

void Entity::update(const float& dt)
{

}

void Entity::render(sf::RenderTarget* target)
{
	
	target->draw(this->sprite);
	
}
