// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
General code cleaning:
Changes made:
	NOTE:	Some changes made related to code cleaning check previous projects to spot changes
			mainly pointers changed to references
			Also as MovementComponent class responsible for moving sprites

	MovementComponent::
	--	Sprite created that wne move() called ssprite is directly affected
	--	Constructor updated
	--	move function updated with delta time

	Entity::
	--	create_component() changed to set_texture()
	--	some pointers changet to references
	--	some changes made to adjust to previous changes
*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}