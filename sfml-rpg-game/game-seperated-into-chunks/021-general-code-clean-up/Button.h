#pragma once

enum button_states {
	BTN_IDLE = 0,
	BTN_HOVER = 1,
	BTN_ACTIVE = 2
};

class Button
{
private:
	// Variables
	short unsigned button_state;

	sf::RectangleShape shape;
	sf::Font* font;
	sf::Text text;

	sf::Color idle_color;
	sf::Color hover_color;
	sf::Color active_color;

public:
	// Construcors & Destructors
	Button(
		float x, float y, 
		float width, float height, 
		sf::Font* font, std::string text,
		sf::Color idle_color, sf::Color hover_color, sf::Color active_color
	);
	~Button();

	// Accessors
	const bool is_pressed() const;

	// Functions
	// Update Functions
	void update(const sf::Vector2f mouse_pos);
	// Render Functions
	void render(sf::RenderTarget* target);
};

