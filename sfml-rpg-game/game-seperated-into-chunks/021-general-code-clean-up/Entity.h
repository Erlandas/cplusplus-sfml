#pragma once
#include "MovementComponent.h"
/*
* Entity class is a base for any type of entity we have
*/
class Entity
{
private:

	// Initialisation blocks/functions
	void init_variables();

protected:
	sf::Sprite sprite;

	MovementComponent* movement_component;

public:
	// Constructors & Destructors
	Entity();
	virtual ~Entity();

	// Component Functions
	void set_texture(sf::Texture& texture);
	void create_movement_component(const float max_velocity);

	// Functions
	virtual void set_position(const float x, const float y);
	virtual void move(const float& dt, const float dir_x, const float dir_y);

	virtual void update(const float& dt);
	virtual void render(sf::RenderTarget* target);
};

