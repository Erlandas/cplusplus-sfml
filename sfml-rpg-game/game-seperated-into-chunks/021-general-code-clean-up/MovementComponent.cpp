#include "pch.h"
#include "MovementComponent.h"

// Initialisation blocks/functions ============================================================

// Constructors & Destructors =================================================================
MovementComponent::MovementComponent(sf::Sprite& sprite, float max_velocity)
	: sprite (sprite), max_velocity (max_velocity)
{
}

MovementComponent::~MovementComponent()
{
}

// Accessors ==================================================================================
const sf::Vector2f& MovementComponent::get_velocity() const
{
	return this->velocity;
}

// Functions ==================================================================================
void MovementComponent::move(const float dir_x, const float dir_y, const float& dt)
{
	this->velocity.x = this->max_velocity * dir_x;
	this->velocity.y = this->max_velocity * dir_y;

	this->sprite.move(
		this->velocity * dt // uses velocity
	);
}

void MovementComponent::update(const float& dt)
{

}
