#include "pch.h"
#include "GameState.h"
// Initialisation blocks/functions ============================================================
void GameState::init_keybinds()
{
	this->keybinds.emplace("MOVE_LEFT", this->supported_keys->at("A"));
	this->keybinds.emplace("MOVE_RIGHT", this->supported_keys->at("D"));
	this->keybinds.emplace("MOVE_UP", this->supported_keys->at("W"));
	this->keybinds.emplace("MOVE_DOWN", this->supported_keys->at("S"));
}

// Construcors & Destructors ==================================================================
GameState::GameState(sf::RenderWindow* window, std::map<std::string, int>* supported_keys)
	: State (window, supported_keys)
{
	this->init_keybinds();
}

GameState::~GameState()
{
}

// Public Functions ===========================================================================

void GameState::end_state()
{
	std::cout << "Ending game state!!\n";
}

//	--	Update Functions ----------------------------------------------------------------------
void GameState::update_input(const float& dt)
{
	this->check_for_quit();

	// Update player input
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("MOVE_LEFT"))))
	{
		this->player.move(dt, -1.f, -0.f);
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("MOVE_RIGHT"))))
	{
		this->player.move(dt, 1.f, -0.f);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("MOVE_UP"))))
	{
		this->player.move(dt, 0.f, -1.f);
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("MOVE_DOWN"))))
	{
		this->player.move(dt, 0.f, 1.f);
	}
}

void GameState::update(const float& dt)
{
	this->update_input(dt);
	
	this->player.update(dt);
}

//	--	Render Functions ----------------------------------------------------------------------
void GameState::render(sf::RenderTarget* target)
{
	if (!target)
		target = this->window;

	this->player.render(target);
}
