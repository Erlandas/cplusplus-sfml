// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Keybinds and input
Changes made:
	In every class find and replaced update_key_binds with update_input

	GameState class:
	--	updated 'void update_input()'
	--	updated 'void GameState::render(sf::RenderTarget* target)'
	--	added 'void GameState::init_keybinds()'
	--	Updated constructor

	Game class:
	--	Added map that holds supported keys 'std::map<std::string, int> supported_keys'
	--	Added 'void init_keys()'
	--	Updated 'void Game::init_states()'

	State class:
	--	Updated 'State(sf::RenderWindow* window, std::map<std::string, int>* suppurted_keys)'
	--	Added 'std::map<std::string, int> keybinds' each state will have its own keybinds
	--	Added 'virtual void init_keybinds() = 0;'

*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}