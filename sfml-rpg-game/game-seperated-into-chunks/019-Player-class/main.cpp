// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Player class

NOTE:	Mistake fixed in entity class

Changes made:
	Entity::
	--	Added function 'virtual void set_position(const float x, const float y)'

	State::
	-- vector for texture* changed to map of <strings, texture >

	GameState::
	-- added init_textures()
	-- added void init_players();
*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}