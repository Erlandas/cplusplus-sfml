#pragma once
/*
* Entity class is a base for any type of entity we have
*/
class Entity
{
private:

	// Initialisation blocks/functions
	void init_variables();

protected:
	sf::Texture* texture;
	sf::Sprite* sprite;

	float movement_speed;

public:
	// Constructors & Destructors
	Entity();
	virtual ~Entity();

	// Component Functions
	void create_sprite(sf::Texture* texture);

	// Functions
	virtual void set_position(const float x, const float y);
	virtual void move(const float& dt, const float dir_x, const float dir_y);

	virtual void update(const float& dt);
	virtual void render(sf::RenderTarget* target);
};

