#pragma once
#include "Entity.h"
class Player :
    public Entity
{
private:
    // Variables

    // Initialisation blocks/functions
    void init_variables();
    void init_components();
public:
    // Construcors & Destructors
    Player(float x, float y, sf::Texture* texture);
    virtual ~Player();

    // Public Functions

    // Update Functions

    // Render Functions
};

