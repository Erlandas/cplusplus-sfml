#include "pch.h"
#include "Entity.h"

// Initialisation blocks/functions ============================================================
void Entity::init_variables()
{
	this->texture = nullptr;
	this->sprite = nullptr;
	this->movement_speed = 100.f;
}
// Constructors & Destructors =================================================================
Entity::Entity()
{
	this->init_variables();
	
}

Entity::~Entity()
{
	delete this->sprite;
}

// Component Functions ========================================================================
void Entity::create_sprite(sf::Texture* texture)
{
	this->texture = texture;
	this->sprite = new sf::Sprite(*this->texture);
	
}

// Functions ==================================================================================
void Entity::set_position(const float x, const float y)
{
	if (this->sprite)
	{
		this->sprite->setPosition(x, y);
	}
	
}

void Entity::move(const float& dt, const float dir_x, const float dir_y)
{
	if (this->sprite)
	{
		this->sprite->move(
				dir_x * this->movement_speed * dt,
				dir_y * this->movement_speed * dt);
	}
	
}

void Entity::update(const float& dt)
{

}

void Entity::render(sf::RenderTarget* target)
{
	if (this->sprite)
	{
		target->draw(*this->sprite);
	}
	
}
