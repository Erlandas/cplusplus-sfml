// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Making buttons click
Changes made:
	Button::
	--	update function improved
	--	render function improved
	--	positioning fixed
	--	enum added for button states

	MainMenuState::
	--	Test button created
	--	Updated with button parameters functions
		+	constructor
		+	update
		+	render

*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}