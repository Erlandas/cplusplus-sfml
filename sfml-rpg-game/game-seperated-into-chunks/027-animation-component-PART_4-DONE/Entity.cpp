#include "pch.h"
#include "Entity.h"

// Initialisation blocks/functions ============================================================
void Entity::init_variables()
{
	this->movement_component = nullptr;
}
// Constructors & Destructors =================================================================
Entity::Entity()
{
	this->init_variables();
	
}

Entity::~Entity()
{
	delete this->movement_component;
	delete this->animation_component;
}

// Component Functions ========================================================================
void Entity::set_texture(sf::Texture& texture)
{
	this->sprite.setTexture(texture);
	
}

void Entity::create_movement_component(
	const float max_velocity,
	const float acceleration,
	const float deceleration
)
{
	this->movement_component = 
		new MovementComponent(this->sprite, max_velocity, acceleration, deceleration);
}

void Entity::create_animation_component(sf::Texture& texture_sheet)
{
	this->animation_component = new AnimationComponent(this->sprite, texture_sheet);
}

// Functions ==================================================================================
void Entity::set_position(const float x, const float y)
{

	this->sprite.setPosition(x, y);

}

void Entity::move( const float dir_x, const float dir_y, const float& dt)
{
	if (this->movement_component)
	{
		this->movement_component->move(dir_x, dir_y, dt); // Sets velocity
	}
	
}

void Entity::update(const float& dt)
{

}

void Entity::render(sf::RenderTarget* target)
{
	
	target->draw(this->sprite);
	
}
