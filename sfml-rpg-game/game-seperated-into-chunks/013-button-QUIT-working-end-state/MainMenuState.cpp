#include "pch.h"
#include "MainMenuState.h"

void MainMenuState::init_fonts()
{
	if (!this->font.loadFromFile("Fonts/Dosis-Light.ttf"))
	{
		throw("ERROR::MAINMENUSTATE::INIT_FONTS::COULD NOT LOAD FONT");
	}
}

// Initialisation blocks/functions ============================================================
void MainMenuState::init_keybinds()
{
	// Open file to read in keys
	/*
	* File format name number representing key
	*   A 0
		D 3
		W 22
		S 18
		Escape 36
	*/
	std::ifstream ifs("Config/gamestate_keybinds.ini");

	if (ifs.is_open())
	{
		std::string key = "";
		std::string key2 = "";
		while (ifs >> key >> key2)
		{
			this->keybinds[key] = this->supported_keys->at(key2);
		}

	}

}

void MainMenuState::init_buttons()
{
	this->buttons["GAME_STATE_BTN"] = new Button(
		100, 100,
		150, 50, 
		&this->font, "New Game",
		sf::Color(70,70,70,200),
		sf::Color(150, 150, 150, 255),
		sf::Color(20, 20, 20, 200)
	);

	this->buttons["EXIT_STATE_BTN"] = new Button(
		100, 300,
		150, 50,
		&this->font, "QUIT",
		sf::Color(100, 100, 100, 200),
		sf::Color(150, 150, 150, 255),
		sf::Color(20, 20, 20, 200)
	);
}

// Construcors & Destructors ==================================================================
MainMenuState::MainMenuState(sf::RenderWindow* window, std::map<std::string, int>* supported_keys)
	: State(window, supported_keys)
{
	this->init_fonts();
	this->init_keybinds();
	this->init_buttons();



	this->background.setSize(sf::Vector2f(
		window->getSize().x, 
		window->getSize().y
	));
	this->background.setFillColor(sf::Color::Blue);
}

MainMenuState::~MainMenuState()
{
	auto it = this->buttons.begin();
	for (it = this->buttons.begin(); it != this->buttons.end(); ++it)
	{
		delete it->second;
	}
}

// Public Functions ===========================================================================

void MainMenuState::end_state()
{
	std::cout << "Ending game state!!\n";
}

//	--	Update Functions ----------------------------------------------------------------------
void MainMenuState::update_input(const float& dt)
{
	this->check_for_quit();

}

void MainMenuState::update_buttons()
{
	//this->game_state_button->update(this->mouse_position_view);
	for (auto& it : this->buttons)
	{
		it.second->update(this->mouse_position_view);
	}

	// NEW game on click
	if (this->buttons["GAME_STATE_BTN"]->is_pressed())
	{
		//this->states.push(new MainMenuState(this->window, &this->supported_keys));
	}

	// EXIT game on click
	if (this->buttons["EXIT_STATE_BTN"]->is_pressed())
	{
		this->quit = true;
	}
}

void MainMenuState::update(const float& dt)
{
	this->update_mouse_positions();
	this->update_input(dt);
	this->update_buttons();
	

	
}



//	--	Render Functions ----------------------------------------------------------------------
void MainMenuState::render_buttons(sf::RenderTarget* target)
{
	//this->game_state_button->render(target);
	for (auto& it : this->buttons)
	{
		it.second->render(target);
	}
}

void MainMenuState::render(sf::RenderTarget* target)
{
	if (!target)
		target = this->window;

	target->draw(this->background);
	this->render_buttons(target);
}
