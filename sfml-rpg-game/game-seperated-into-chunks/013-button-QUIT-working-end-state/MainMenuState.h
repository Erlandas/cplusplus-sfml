#pragma once
#include "GameState.h"
#include "Button.h"
class MainMenuState :
    public State
{
private:
    // Variables
    sf::RectangleShape background;
    sf::Font font;

    std::map<std::string, Button*> buttons;

    // Initialisation blocks/functions
    void init_fonts();
    void init_keybinds();
    void init_buttons();
public:
    // Construcors & Destructors
    MainMenuState(sf::RenderWindow* window, std::map<std::string, int>* supported_keys);
    virtual ~MainMenuState();

    // Public Functions

    void end_state();
    //	--	Update Functions
    void update_input(const float& dt);
    void update_buttons();
    void update(const float& dt);
    //	--	Render Functions
    void render_buttons(sf::RenderTarget* target = nullptr);
    void render(sf::RenderTarget* target = nullptr);


};

