// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Working buttons QUIT
Changes made:
	Button::

	MainMenuState::
	--	function created 'void init_buttons()'
	--	map created that holds < string, button* >
	--	function created update_buttons()
	--	created render_buttons(sf::RenderTarget* target = nullptr)

*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}