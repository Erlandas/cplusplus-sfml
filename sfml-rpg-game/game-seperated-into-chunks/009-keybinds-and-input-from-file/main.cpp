// main.cpp : This file contains the 'main' function. Program execution begins and ends there.
/*

Keybinds and input from file:
changes made:
	Everywhere where i am adding elements to a map way chaged from emplace to regular way

	State class:
		--	Using keybinds to close window 'void State::check_for_quit()'

	Importing keys  from files:
		--	Supported keys imported in a Game::init_keys()
		--	Keybinds imported in a void GameState::init_keybinds()

*/
#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}