#include "pch.h"
#include "State.h"
// Construcors & Destructors ==================================================================
State::State(sf::RenderWindow* window, std::map<std::string, int>* supported_keys)
{
	this->window = window;
	this->supported_keys = supported_keys;
	this->quit = false;
}

State::~State()
{

}



// Public Functions ==========================================================================
const bool& State::get_quit() const
{
	return this->quit;
}

void State::check_for_quit()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("CLOSE"))))
	{
		this->quit = true;
	}
}
