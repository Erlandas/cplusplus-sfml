// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Polishing buttons
Changes made:
	MainMenuState::
	--	Button added for editor
	--	Buttons changed to match updated constructor

	Button::
	--	Constructor update with 4 extra arguments
	--	Update() updated that changes text color


*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}