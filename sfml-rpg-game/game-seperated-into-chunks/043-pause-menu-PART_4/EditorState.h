#pragma once
#include "State.h"
#include "Button.h"

class EditorState :
    public State
{

private:
    // Variables
    sf::Font font;

    std::map<std::string, Button*> buttons;

    // Initialisation blocks/functions
    void init_variables();
    void init_background();
    void init_fonts();
    void init_keybinds();
    void init_buttons();
public:
    // Construcors & Destructors
    EditorState(
        sf::RenderWindow* window,
        std::map<std::string,
        int>* supported_keys,
        std::stack<State*>* states
    );
    virtual ~EditorState();

    // Public Functions

    //	--	Update Functions
    void update_input(const float& dt);
    void update_buttons();
    void update(const float& dt);
    //	--	Render Functions
    void render_buttons(sf::RenderTarget& target);
    void render(sf::RenderTarget* target = nullptr);
};

