// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Getting info from movement component for animation
Changes made:
	MovementComponent::
	--	Function added get_state()
	--	Enumeration added that indicates direction of player movement
			

*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}