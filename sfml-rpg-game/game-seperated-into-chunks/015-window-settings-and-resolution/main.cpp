// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Window Settings and Resolution

NOTE: Now im looking at every resolution (video modes) my computer supports
	and i will choose between those

Changes made:
	Game::
	--	Added vector variable that holds supported video modes
	--	Updated init_window()
		+	initialised video modes vector
		+	boolean for fullscreen
		+	unsigned antialising level variable
	--	Updated window.ini file
	
*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}