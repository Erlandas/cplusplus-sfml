#include "pch.h"
#include "State.h"
// Construcors & Destructors ==================================================================
State::State(
	sf::RenderWindow* window, 
	std::map<std::string, 
	int>* supported_keys, 
	std::stack<State*>* states)
{
	this->window = window;
	this->supported_keys = supported_keys;
	this->states = states;
	this->quit = false;
}

State::~State()
{

}



// Public Functions ==========================================================================
const bool& State::get_quit() const
{
	return this->quit;
}

void State::check_for_quit()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("CLOSE"))))
	{
		this->quit = true;
	}
}

// Update Functions ==========================================================================
void State::update_mouse_positions()
{
	this->mouse_position_screen = sf::Mouse::getPosition();
	this->mouse_position_window = sf::Mouse::getPosition(*this->window);
	this->mouse_position_view = this->window->mapPixelToCoords(sf::Mouse::getPosition(*this->window));
}
