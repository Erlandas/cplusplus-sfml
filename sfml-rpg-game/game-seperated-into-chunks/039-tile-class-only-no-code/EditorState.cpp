#include "pch.h"
#include "EditorState.h"

// Initialisation blocks/functions ============================================================
void EditorState::init_variables()
{

}

void EditorState::init_background()
{

}

void EditorState::init_fonts()
{
	if (!this->font.loadFromFile("Fonts/Dosis-Light.ttf"))
	{
		throw("ERROR::EDITORSTATE::INIT_FONTS::COULD NOT LOAD FONT");
	}
}

void EditorState::init_keybinds()
{
	// Open file to read in keys
	/*
	* File format name number representing key
	*   A 0
		D 3
		W 22
		S 18
		Escape 36
	*/
	std::ifstream ifs("Config/editorstate_keybinds.ini");

	if (ifs.is_open())
	{
		std::string key = "";
		std::string key2 = "";
		while (ifs >> key >> key2)
		{
			this->keybinds[key] = this->supported_keys->at(key2);
		}

	}

}

void EditorState::init_buttons()
{

}

// Construcors & Destructors ==================================================================
EditorState::EditorState(
	sf::RenderWindow* window,
	std::map<std::string,
	int>* supported_keys,
	std::stack<State*>* states
)
	: State(window, supported_keys, states)
{
	this->init_variables();
	this->init_background();
	this->init_fonts();
	this->init_keybinds();
	this->init_buttons();

}

EditorState::~EditorState()
{
	auto it = this->buttons.begin();
	for (it = this->buttons.begin(); it != this->buttons.end(); ++it)
	{
		delete it->second;
	}
}

// Public Functions ===========================================================================

//	--	Update Functions ----------------------------------------------------------------------
void EditorState::update_input(const float& dt)
{

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("CLOSE"))))
	{
		this->end_state();
	}
}

void EditorState::update_buttons()
{
	//this->game_state_button->update(this->mouse_position_view);
	for (auto& it : this->buttons)
	{
		it.second->update(this->mouse_position_view);
	}

}

void EditorState::update(const float& dt)
{
	this->update_mouse_positions();
	this->update_input(dt);
	this->update_buttons();



}



//	--	Render Functions ----------------------------------------------------------------------
void EditorState::render_buttons(sf::RenderTarget& target)
{
	//this->game_state_button->render(target);
	for (auto& it : this->buttons)
	{
		it.second->render(target);
	}
}

void EditorState::render(sf::RenderTarget* target)
{
	if (!target)
		target = this->window;

	this->render_buttons(*target);

	//REMOVE LATER!!!
	/*
	* Display text that shows us mouse position all the time
	* just checking where to put buttons
	* For DEBUGGING check where to place button or other objects
	*/
	//sf::Text mouse_text;
	//mouse_text.setPosition(this->mouse_position_view.x, this->mouse_position_view.y -50);
	//mouse_text.setFont(this->font);
	//mouse_text.setCharacterSize(12);
	//std::stringstream ss;
	//ss << this->mouse_position_view.x << " " << this->mouse_position_view.y;
	//mouse_text.setString(ss.str());
	//target->draw(mouse_text);
}
