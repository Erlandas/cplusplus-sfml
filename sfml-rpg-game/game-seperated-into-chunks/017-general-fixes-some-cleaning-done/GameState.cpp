#include "pch.h"
#include "GameState.h"
// Initialisation blocks/functions ============================================================
void GameState::init_keybinds()
{
	// Open file to read in keys
	/*
	* File format name number representing key
	*   A 0
		D 3
		W 22
		S 18
		Escape 36
	*/
	std::ifstream ifs("Config/gamestate_keybinds.ini");

	if (ifs.is_open())
	{
		std::string key = "";
		std::string key2 = "";
		while (ifs >> key >> key2)
		{
			this->keybinds[key] = this->supported_keys->at(key2);
		}

	}

}

// Construcors & Destructors ==================================================================
GameState::GameState(
	sf::RenderWindow* window, 
	std::map<std::string, 
	int>* supported_keys,
	std::stack<State*>* states
)
	: State (window, supported_keys, states)
{
	this->init_keybinds();
}

GameState::~GameState()
{
}

// Public Functions ===========================================================================

//	--	Update Functions ----------------------------------------------------------------------
void GameState::update_input(const float& dt)
{

	// Update player input
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("MOVE_LEFT"))))
	{
		this->player.move(dt, -1.f, -0.f);
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("MOVE_RIGHT"))))
	{
		this->player.move(dt, 1.f, -0.f);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("MOVE_UP"))))
	{
		this->player.move(dt, 0.f, -1.f);
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("MOVE_DOWN"))))
	{
		this->player.move(dt, 0.f, 1.f);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("CLOSE"))))
	{
		this->end_state();
	}
}

void GameState::update(const float& dt)
{
	this->update_mouse_positions();

	this->update_input(dt);
	
	this->player.update(dt);
}

//	--	Render Functions ----------------------------------------------------------------------
void GameState::render(sf::RenderTarget* target)
{
	if (!target)
		target = this->window;

	this->player.render(target);
}
