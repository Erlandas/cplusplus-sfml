// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
General engine fixes some cleaning done
Changes made:
	State::
	--	Fully removed method check_for_quit()
	--	end_state() changed to not pure virtual and implemented

	NOTE:	removed check_for_quit() method calls from any state related classes

	GameState::
	--	changes in update_input() quit functionality
	
*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}