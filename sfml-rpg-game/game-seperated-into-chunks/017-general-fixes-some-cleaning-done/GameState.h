#pragma once
#include "State.h"

class GameState :
    public State
{
private:
    Entity player;

    // Initialisation blocks/functions
    void init_keybinds();
public:
    // Construcors & Destructors
    GameState(
        sf::RenderWindow* window, 
        std::map<std::string, 
        int>* supported_keys,
        std::stack<State*>* states
    );
    virtual ~GameState();

    // Public Functions
    
    //	--	Update Functions
    void update_input(const float& dt);
    void update(const float& dt);
    //	--	Render Functions
    void render(sf::RenderTarget* target = nullptr);
};

