#include "pch.h"
#include "State.h"
// Construcors & Destructors ==================================================================
State::State(
	sf::RenderWindow* window, 
	std::map<std::string, 
	int>* supported_keys, 
	std::stack<State*>* states)
{
	this->window = window;
	this->supported_keys = supported_keys;
	this->states = states;
	this->quit = false;
	this->paused = false;

	this->key_time = 0.f;
	this->key_time_max = 10.f;
}

State::~State()
{

}



// ACCESSORS ===================================================================================

const bool& State::get_quit() const
{
	return this->quit;
}

const bool State::get_keyt_time()
{
	if (this->key_time >= this->key_time_max)
	{
		this->key_time = 0.f;
		return true;
	}
	return false;
}

// FUNCTIONS
void State::end_state()
{
	this->quit = true;
}



// PAUSE related ===============================================================================
void State::pause_state()
{
	this->paused = true;
}

void State::un_pause_state()
{
	this->paused = false;
}

// Update Functions ==========================================================================
void State::update_mouse_positions()
{
	this->mouse_position_screen = sf::Mouse::getPosition();
	this->mouse_position_window = sf::Mouse::getPosition(*this->window);
	this->mouse_position_view = this->window->mapPixelToCoords(sf::Mouse::getPosition(*this->window));
}

void State::update_key_time(const float& dt)
{
	if (this->key_time < this->key_time_max)
	{
		this->key_time += 100.f * dt;
	}
}
