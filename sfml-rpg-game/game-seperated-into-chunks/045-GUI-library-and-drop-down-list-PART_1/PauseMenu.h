#pragma once
#include "Gui.h"
class PauseMenu
{
private:
	sf::Font& font;
	sf::Text menu_text;

	sf::RectangleShape background;
	sf::RectangleShape container;

	std::map<std::string, gui::Button*> buttons;

	// Private functions


public:
	// Constructors & Destructors
	PauseMenu(sf::RenderWindow& window, sf::Font& font);
	virtual ~PauseMenu();

	// Accessor
	std::map<std::string, gui::Button*>& get_buttons();

	// Functions
	const bool is_button_pressed(const std::string key);
	void add_button(
		const std::string key, 
		float y,
		const std::string text);
	void update(const sf::Vector2f& mouse_position);
	void render(sf::RenderTarget& target);
};


