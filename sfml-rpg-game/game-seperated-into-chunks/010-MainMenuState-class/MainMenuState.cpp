#include "pch.h"
#include "MainMenuState.h"

// Initialisation blocks/functions ============================================================
void MainMenuState::init_keybinds()
{
	// Open file to read in keys
	/*
	* File format name number representing key
	*   A 0
		D 3
		W 22
		S 18
		Escape 36
	*/
	std::ifstream ifs("Config/gamestate_keybinds.ini");

	if (ifs.is_open())
	{
		std::string key = "";
		std::string key2 = "";
		while (ifs >> key >> key2)
		{
			this->keybinds[key] = this->supported_keys->at(key2);
		}

	}

}

// Construcors & Destructors ==================================================================
MainMenuState::MainMenuState(sf::RenderWindow* window, std::map<std::string, int>* supported_keys)
	: State(window, supported_keys)
{
	this->init_keybinds();
	this->background.setSize(sf::Vector2f(
		window->getSize().x, 
		window->getSize().y
	));
	this->background.setFillColor(sf::Color::Blue);
}

MainMenuState::~MainMenuState()
{
}

// Public Functions ===========================================================================

void MainMenuState::end_state()
{
	std::cout << "Ending game state!!\n";
}

//	--	Update Functions ----------------------------------------------------------------------
void MainMenuState::update_input(const float& dt)
{
	this->check_for_quit();

}

void MainMenuState::update(const float& dt)
{
	this->update_input(dt);
}

//	--	Render Functions ----------------------------------------------------------------------
void MainMenuState::render(sf::RenderTarget* target)
{
	if (!target)
		target = this->window;

	target->draw(this->background);
}
