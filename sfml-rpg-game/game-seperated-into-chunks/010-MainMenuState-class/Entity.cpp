#include "pch.h"
#include "Entity.h"
// Constructors & Destructors =================================================================
Entity::Entity()
{
	this->shape.setSize(sf::Vector2f(50.f, 50.f));
	this->movement_speed = 100.f;
}

Entity::~Entity()
{
}

// Functions ==================================================================================
void Entity::move(const float& dt, const float dir_x, const float dir_y)
{
	this->shape.move(
		dir_x * this->movement_speed * dt,
		dir_y * this->movement_speed * dt);
}

void Entity::update(const float& dt)
{

}

void Entity::render(sf::RenderTarget* target)
{
	target->draw(this->shape);
}
