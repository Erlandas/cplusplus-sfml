// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Mian menu state
Changes made:
	MainMenuState class created, to NOTE it is nearly direct copy of GameState class.

	Game class:
		--	Included created new main menu state class
		--	Changes made in 'void Game::init_states()'

*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}