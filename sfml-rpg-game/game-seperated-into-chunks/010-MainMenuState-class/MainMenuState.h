#pragma once
#include "GameState.h"
class MainMenuState :
    public State
{
private:
    sf::RectangleShape background;

    // Initialisation blocks/functions
    void init_keybinds();
public:
    // Construcors & Destructors
    MainMenuState(sf::RenderWindow* window, std::map<std::string, int>* supported_keys);
    virtual ~MainMenuState();

    // Public Functions

    void end_state();
    //	--	Update Functions
    void update_input(const float& dt);
    void update(const float& dt);
    //	--	Render Functions
    void render(sf::RenderTarget* target = nullptr);


};

