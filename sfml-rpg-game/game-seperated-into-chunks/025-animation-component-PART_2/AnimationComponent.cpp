#include "pch.h"
#include "AnimationComponent.h"

// Constructors & Destructors =================================================================
AnimationComponent::AnimationComponent(sf::Sprite& sprite, sf::Texture& texture_sheet)
	: sprite(sprite), texture_sheet(texture_sheet)
{

}

AnimationComponent::~AnimationComponent()
{
}
// Functions ==================================================================================

// --	Animation Related Functions ===========================================================
void AnimationComponent::start_animation(const std::string animation)
{
}

void AnimationComponent::pause_animation(const std::string animation)
{
}

void AnimationComponent::reset_animation(const std::string animation)
{
}

// --	Update Functions ======================================================================
void AnimationComponent::update(const float& dt)
{
}

