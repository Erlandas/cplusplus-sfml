// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Animation Component
<<PART 2>>
Changes made:
	NOTE:	Building upon both previous classes (AnimationComponent, Animation)
			Observe changes made between PART 1 and PART 2

	AnimationComponent::
	--	Constructor updated
	--	Added extra functions:
			void add_animation(const std::string key);
			void start_animation(const std::string animation);
			void pause_animation(const std::string animation);
			void reset_animation(const std::string animation);

	AnimationComponent::Animation
	--	update() updated
	--	Constructor updated

<<PART 1>>
Changes made:
	NOTE:	When we instantiate entities those entities wont have textures,
			When we add AnimationComponent to entity then this entity will 
			have animation

	--	AnimationComponent class created
	--	Created Animation class within AnimationComponent class
		this way we can have Animations within AnimationComponent
	--	Blueprint in both class are in .h file check those
	--	No major definitions at this moment
*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}