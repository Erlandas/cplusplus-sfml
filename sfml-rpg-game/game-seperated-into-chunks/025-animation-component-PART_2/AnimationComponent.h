#pragma once
class AnimationComponent
{
private:
	// Classes
	class Animation
	{
	public:
		// Variables
		sf::Sprite& sprite;
		sf::Texture& texture_sheet;
		float animation_timer;
		float timer;
		int width;
		int height;
		sf::IntRect start_rect;
		sf::IntRect current_rect;
		sf::IntRect end_rect;

		Animation(
			sf::Sprite& sprite, sf::Texture& texture_sheet, 
			float animation_timer,
			int start_x, int start_y, int end_x, int end_y, int width, int height)

			: sprite(sprite), texture_sheet(texture_sheet), animation_timer(animation_timer),
			width(width), height(height)
		{
			this->start_rect = sf::IntRect(start_x, start_y, this->width, this->height);
			this->current_rect = this->start_rect;
			this->end_rect = sf::IntRect(end_x, end_y, this->width, this->height);

			this->sprite.setTexture(this->texture_sheet, true);
			this->sprite.setTextureRect(this->start_rect);
		}

		// Functions
		void update(const float& dt) 
		{
			// Update timer
			this->timer = 10.f * dt;
			if (this->timer >= this->animation_timer)
			{
				// Reset timer
				this->timer = 0.f;

				// Animate
				if (this->current_rect != this->end_rect)
				{
					this->current_rect.left += this->width;
				} 
				// Reset
				else
				{
					this->current_rect.left = this->start_rect.left;
				}
			}
		}
		void pause();
		void reset();
	};

	// Variables
	sf::Sprite& sprite;
	sf::Texture& texture_sheet;
	std::map<std::string, Animation> animations;

	// Initialisation blocks/functions
public:
	// Constructors & Destructors
	AnimationComponent(sf::Sprite& sprite, sf::Texture& texture_sheet);
	virtual ~AnimationComponent();

	// Functions
	void add_animation(const std::string key);

	void start_animation(const std::string animation);
	void pause_animation(const std::string animation);
	void reset_animation(const std::string animation);

	void update(const float& dt);
};

