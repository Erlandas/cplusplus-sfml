// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Multiple animations and a nice texture
Changes made:
	Player::
	--	Animation added for left walking in Player()

	MovementComponent::
	--	Added function 'const bool MovementComponent::idle() const'
*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}