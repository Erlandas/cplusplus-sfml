// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Our own GUI library and drop down list
<<PART 2>>
	gui::Button::
	--	Implemented functions
		+	const std::string& get_text() const;
		+	void set_text(const std::string text);

	gui::DropDownList::
	--	Cnstructor updated

<<PART 1>>
NOTE:	Button class assigned to namespace, all calls for button changed to gui::Button
		We can use "using namespace gui" but i like to see what i am using it from

NOTE:	We using gui:: namespace to create gui related elements so all headers of classes related
		to gui will be heald in a single file under gui namespace
		it will be eisier to work as we seperate gui elements

NOTE:	Button class renamed to Gui as it will be holding all GUI related elements

	Changes made:
	--	Class DropDownlist created under namespace gui
*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}