// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Settings State
Changes made:
	NOTE:	Settings state created
		Direct copy of MainMenuState class
		With few changes made
	
*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}