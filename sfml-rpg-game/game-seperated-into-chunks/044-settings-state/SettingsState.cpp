#include "pch.h"
#include "SettingsState.h"

// Initialisation blocks/functions =============================================================
void SettingsState::init_variables()
{

}

void SettingsState::init_background()
{
	this->background.setSize(sf::Vector2f(
		static_cast<float>(this->window->getSize().x),
		static_cast<float>(this->window->getSize().y)
	));

	if (!this->background_texture.loadFromFile("Resources/Images/Backgrounds/bg1.png"))
	{
		throw "!! ERROR::MAIN_MENU_STATE::FAILED_TO_LOAD_BACKGROUND";
	}

	this->background.setTexture(&this->background_texture);

}

void SettingsState::init_fonts()
{
	if (!this->font.loadFromFile("Fonts/Dosis-Light.ttf"))
	{
		throw("ERROR::MAINMENUSTATE::INIT_FONTS::COULD NOT LOAD FONT");
	}
}

void SettingsState::init_keybinds()
{
	// Open file to read in keys
	/*
	* File format name number representing key
	*   A 0
		D 3
		W 22
		S 18
		Escape 36
	*/
	std::ifstream ifs("Config/mainmenustate_keybinds.ini");

	if (ifs.is_open())
	{
		std::string key = "";
		std::string key2 = "";
		while (ifs >> key >> key2)
		{
			this->keybinds[key] = this->supported_keys->at(key2);
		}

	}

}

void SettingsState::init_buttons()
{
	this->buttons["EXIT_STATE_BTN"] = new Button(
		300.f, 880.f,
		250.f, 50.f,
		&this->font, "QUIT", 50,
		sf::Color(100, 100, 100, 200), sf::Color(250, 250, 250, 250), sf::Color(20, 20, 20, 50),
		sf::Color(255, 100, 100, 0), sf::Color(255, 150, 150, 0), sf::Color(255, 20, 20, 0)
	);
}

// Constructor & Destructors ===================================================================
SettingsState::SettingsState(
    sf::RenderWindow* window,
    std::map<std::string,
    int>* supported_keys,
    std::stack<State*>* states
)
	: State(window, supported_keys, states)
{
	this->init_variables();
	this->init_background();
	this->init_fonts();
	this->init_keybinds();
	this->init_buttons();
}

SettingsState::~SettingsState()
{
	auto it = this->buttons.begin();
	for (it = this->buttons.begin(); it != this->buttons.end(); ++it)
	{
		delete it->second;
	}
}

// Accessors
// Public Functions ===========================================================================

//	--	Update Functions ----------------------------------------------------------------------
void SettingsState::update_input(const float& dt)
{


}

void SettingsState::update_buttons()
{
	//this->game_state_button->update(this->mouse_position_view);
	for (auto& it : this->buttons)
	{
		it.second->update(this->mouse_position_view);
	}

	// EXIT game on click
	if (this->buttons["EXIT_STATE_BTN"]->is_pressed())
	{
		this->end_state();
	}
}

void SettingsState::update(const float& dt)
{
	this->update_mouse_positions();
	this->update_input(dt);
	this->update_buttons();



}

//	--	Render Functions ----------------------------------------------------------------------
void SettingsState::render_buttons(sf::RenderTarget& target)
{
	//this->game_state_button->render(target);
	for (auto& it : this->buttons)
	{
		it.second->render(target);
	}
}

void SettingsState::render(sf::RenderTarget* target)
{
	if (!target)
		target = this->window;

	target->draw(this->background);
	this->render_buttons(*target);

	//REMOVE LATER!!!
	/*
	* Display text that shows us mouse position all the time
	* just checking where to put buttons
	* For DEBUGGING check where to place button or other objects
	*/
	//sf::Text mouse_text;
	//mouse_text.setPosition(this->mouse_position_view.x, this->mouse_position_view.y -50);
	//mouse_text.setFont(this->font);
	//mouse_text.setCharacterSize(12);
	//std::stringstream ss;
	//ss << this->mouse_position_view.x << " " << this->mouse_position_view.y;
	//mouse_text.setString(ss.str());
	//target->draw(mouse_text);
}
