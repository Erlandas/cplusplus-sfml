// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Hit Box Component

NOTE:	Collision detection component

Changes made:
	--	HitboxComponent class created
	--	included in entity
	--	added to entity

*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}