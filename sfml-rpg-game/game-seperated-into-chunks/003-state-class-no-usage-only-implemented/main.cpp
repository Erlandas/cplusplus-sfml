// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
    Defined state class no usage yet just state class
    Using pure virtual functions for inheritance
*/
#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}