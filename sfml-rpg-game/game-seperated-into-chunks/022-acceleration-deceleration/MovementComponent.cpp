#include "pch.h"
#include "MovementComponent.h"

// Initialisation blocks/functions ============================================================

// Constructors & Destructors =================================================================
MovementComponent::MovementComponent(
	sf::Sprite& sprite, 
	float max_velocity,
	float acceleration,
	float deceleration
)
	: sprite (sprite), 
	max_velocity (max_velocity), 
	acceleration (acceleration), 
	deceleration (deceleration)
{
}

MovementComponent::~MovementComponent()
{
}

// Accessors ==================================================================================
const sf::Vector2f& MovementComponent::get_velocity() const
{
	return this->velocity;
}

// Functions ==================================================================================
void MovementComponent::move(const float dir_x, const float dir_y, const float& dt)
{
	/*Accelerating a sprite until it reaches mac velocity*/

	this->velocity.x += this->acceleration * dir_x;

	this->velocity.y += this->acceleration * dir_y;

}

void MovementComponent::update(const float& dt)
{
	/*Decelerates the sprite and checks the maximum velocity.
	Moves the sprite.
	*/
	if (this->velocity.x > 0.f) // check for positive X
	{

		// MAX velocity check X positive
		if (this->velocity.x > this->max_velocity)
		{
			this->velocity.x = this->max_velocity;
		}

		// Deceleration x positive
		this->velocity.x -= deceleration;
		if (this->velocity.x < 0.f)
		{
			this->velocity.x = 0.f;
		}

	}
	else if (this->velocity.x < 0.f) // Check for negative X
	{
		// MAX velocity check X negative
		if (this->velocity.x < -this->max_velocity)
		{
			this->velocity.x = -this->max_velocity;
		}

		// Deceleration x negative
		this->velocity.x += deceleration;
		if (this->velocity.x > 0.f)
		{
			this->velocity.x = 0.f;
		}
	}
	//======================================================
	if (this->velocity.y > 0.f) // check for positive Y
	{

		// MAX velocity check Y positive
		if (this->velocity.y > this->max_velocity)
		{
			this->velocity.y = this->max_velocity;
		}

		// Deceleration Y positive
		this->velocity.y -= deceleration;
		if (this->velocity.y < 0.f)
		{
			this->velocity.y = 0.f;
		}

	}
	else if (this->velocity.y < 0.f) // Check for negative Y
	{
		// MAX velocity check X negative
		if (this->velocity.y < -this->max_velocity)
		{
			this->velocity.y = -this->max_velocity;
		}

		// Deceleration x negative
		this->velocity.y += deceleration;
		if (this->velocity.y > 0.f)
		{
			this->velocity.y = 0.f;
		}
	}

	// Final move
	this->sprite.move(this->velocity * dt);
}
