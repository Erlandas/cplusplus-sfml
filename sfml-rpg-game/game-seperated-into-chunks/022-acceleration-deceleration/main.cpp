// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Acceleration and simple physics for the movement
Changes made:
	Entity::
	--	Changes made Entity::update(const float& dt) 
		update method called for movement component

	MovementComponent::
	--	update() changed
	--	constructor updated
		RELATED constructor calls updated to match argument list from various classes 
	--	acceleration implemented in update()
	--	deceleration implemented in update()
*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}