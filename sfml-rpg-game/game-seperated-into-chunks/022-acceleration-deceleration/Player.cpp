#include "pch.h"
#include "Player.h"

// Initialisation blocks/functions ============================================================
void Player::init_variables()
{
}

void Player::init_components()
{
	this->create_movement_component(300.f, 10.f, 4.f);
}

// Construcors & Destructors ==================================================================
Player::Player(float x, float y, sf::Texture& texture)
{
	this->init_variables();
	this->init_components();
	this->set_texture(texture);
	this->set_position(x, y);
}

Player::~Player()
{
}
