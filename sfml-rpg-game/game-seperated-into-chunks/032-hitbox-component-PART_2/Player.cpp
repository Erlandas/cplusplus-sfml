#include "pch.h"
#include "Player.h"

// Initialisation blocks/functions ============================================================
void Player::init_variables()
{
}

void Player::init_components()
{
	
}

// Construcors & Destructors ==================================================================
Player::Player(float x, float y, sf::Texture& texture_sheet)
{
	this->init_variables();

	this->set_position(x, y);

	this->create_hitbox_component(
		this->sprite, 
		96.f, 66.f,
		86.f, 106.f
	);
	this->create_movement_component(300.f, 10.f, 4.f);
	this->create_animation_component(texture_sheet);

	this->animation_component->add_animation("IDLE_LEFT", 10.f, 0, 0, 13, 0, 192, 192);
	this->animation_component->add_animation("WALK_LEFT", 10.f, 0, 1, 11, 1, 192, 192);
}

Player::~Player()
{
}

// Update Functions ===========================================================================
void Player::update(const float& dt)
{
	this->movement_component->update(dt);

	if (this->movement_component->get_state(IDLE))
	{
		this->animation_component->play("IDLE_LEFT", dt);
	}
	else if (this->movement_component->get_state(MOVING_LEFT))
	{
		this->animation_component->play("WALK_LEFT", dt);
	}

	this->hitbox_component->update();

}
