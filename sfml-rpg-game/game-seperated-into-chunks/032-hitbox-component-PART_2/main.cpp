// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Hit Box Component
NOTE:	seperates collision from the sprite itself
<<PART 2>>
	--	method added in entity that creates component for hitbox
			void create_hitbox_component(
				sf::Sprite& sprite,
				const float offset_x, const float offset_y,
				float width, float height);
	--	many pointer changed to references for target display

<<PART 1>>
NOTE:	Collision detection component

Changes made:
	--	HitboxComponent class created
	--	included in entity
	--	added to entity

*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}