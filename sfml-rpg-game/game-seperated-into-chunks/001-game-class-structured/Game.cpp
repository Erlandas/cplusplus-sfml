#include "pch.h"
#include "Game.h"

// Initialisation blocks/functions ============================================================
void Game::init_window()
{
	this->window = new sf::RenderWindow(sf::VideoMode(800, 600), "C++ SFML RPG");
}

// Construcors & Destructors ==================================================================
Game::Game()
{
    this->init_window();
}

Game::~Game()
{
	delete this->window;
}

// Public Functions ===========================================================================

//	--	Update Functions ----------------------------------------------------------------------
void Game::update_SFML_events()
{
        while (this->window->pollEvent(this->sf_event))
        {
            if (sf_event.type == sf::Event::Closed)
                this->window->close();
            //if (sf_event.type == sf::Event::KeyPressed
            //    && sf_event.key.code == sf::Keyboard::Escape)
        }
}

void Game::update()
{
    this->update_SFML_events();
}

//	--	Render Functions ----------------------------------------------------------------------
void Game::render()
{
    this->window->clear();

    // Render items here

    this->window->display();
}

//	--	Game Loop -----------------------------------------------------------------------------
void Game::run()
{

    while (this->window->isOpen())
    {
        this->update();
        this->render();
    }
}
