#pragma once
class Game
{
private:
	// Variables
	sf::RenderWindow* window;
	sf::Event sf_event;

	// Initialisation blocks/functions
	void init_window();
public:
	// Construcors & Destructors
	Game();
	virtual ~Game();

	// Public Functions

	//	--	Update Functions
	void update_SFML_events();
	void update();
	//	--	Render Functions
	void render();
	//	--	Game Loop
	void run();
};

