// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
    Commented and structured starting point for any game that includes:

        --  Precompiled headers
        --  Game class
        --  Main class
*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}