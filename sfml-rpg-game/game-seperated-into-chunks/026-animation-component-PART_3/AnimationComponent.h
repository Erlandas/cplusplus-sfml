#pragma once
class AnimationComponent
{
private:
	// Classes
	class Animation
	{
	public:
		// Variables
		sf::Sprite& sprite;
		sf::Texture& texture_sheet;
		float animation_timer;
		float timer;
		int width;
		int height;
		sf::IntRect start_rect;
		sf::IntRect current_rect;
		sf::IntRect end_rect;

		Animation(
			sf::Sprite& sprite, sf::Texture& texture_sheet, 
			float animation_timer,
			int start_frame_x, int start_frame_y, int frames_x, int frames_y, int width, int height)

			: sprite(sprite), texture_sheet(texture_sheet), animation_timer(animation_timer),
			width(width), height(height)
		{
			this->start_rect = sf::IntRect(
				start_frame_x * width, start_frame_y * height, 
				this->width, this->height
			);
			this->current_rect = this->start_rect;
			this->end_rect = sf::IntRect(
				frames_x * width, frames_y * height, 
				this->width, this->height
			);

			this->sprite.setTexture(this->texture_sheet, true);
			this->sprite.setTextureRect(this->start_rect);
		}

		// Functions
		void play(const float& dt) 
		{
			// Update timer
			this->timer = 10.f * dt;
			if (this->timer >= this->animation_timer)
			{
				// Reset timer
				this->timer = 0.f;

				// Animate
				if (this->current_rect != this->end_rect)
				{
					this->current_rect.left += this->width;
				} 
				// Reset
				else
				{
					this->current_rect.left = this->start_rect.left;
				}

				this->sprite.setTextureRect(this->current_rect);
			}
		}

		void reset()
		{
			this->timer = 0.f;
			this->current_rect = this->start_rect;
		}
	};

	// Variables
	sf::Sprite& sprite;
	sf::Texture& texture_sheet;
	std::map<std::string, Animation*> animations;

	// Initialisation blocks/functions
public:
	// Constructors & Destructors
	AnimationComponent(sf::Sprite& sprite, sf::Texture& texture_sheet);
	virtual ~AnimationComponent();

	// Functions
	void add_animation(
		const std::string key, 
		float animation_timer,
		int start_frame_x, int start_frame_y, int frames_x, int frames_y, 
		int width, int height
	);
	void play(const std::string key, const float& dt);
};

