#include "pch.h"
#include "Player.h"

// Initialisation blocks/functions ============================================================
void Player::init_variables()
{
}

void Player::init_components()
{
	
}

// Construcors & Destructors ==================================================================
Player::Player(float x, float y, sf::Texture& texture_sheet)
{
	this->init_variables();

	this->set_texture(texture_sheet);
	this->set_position(x, y);

	this->create_movement_component(300.f, 10.f, 4.f);
	this->create_animation_component(this->sprite, texture_sheet);

	this->animation_component->add_animation("IDLE_LEFT", 100.f, 1, 1, 14, 1, 192, 192);
}

Player::~Player()
{
}
