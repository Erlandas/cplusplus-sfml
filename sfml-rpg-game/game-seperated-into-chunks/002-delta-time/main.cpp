// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
    Simple implementation of delta time
    Comments indicates what is related to delta time in a game class
    No usage yet of delta time just starting point
*/
#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}