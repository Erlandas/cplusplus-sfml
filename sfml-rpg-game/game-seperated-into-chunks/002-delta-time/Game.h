#pragma once
class Game
{
private:
	// Variables
	sf::RenderWindow* window;
	sf::Event sf_event;

	//	--	Delta time related

	/*dt short for delta time it is going to keep track of how long it took
	for us for a game to render for up for to do one update call and one render call,
	thats a frame so one update and one render together how long did that take.
	Helps to achieve frame rate independant game.*/

	sf::Clock dt_clock;
	float dt;

	// Initialisation blocks/functions
	void init_window();
public:
	// Construcors & Destructors
	Game();
	virtual ~Game();

	// Public Functions

	//	--	Update Functions
	void update_dt();	//	DELTA TIME RELATED
	void update_SFML_events();
	void update();
	//	--	Render Functions
	void render();
	//	--	Game Loop
	void run();
};

