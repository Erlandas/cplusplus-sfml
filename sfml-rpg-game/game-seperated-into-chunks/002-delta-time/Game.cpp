#include "pch.h"
#include "Game.h"

// Initialisation blocks/functions ============================================================
void Game::init_window()
{
	this->window = new sf::RenderWindow(sf::VideoMode(800, 600), "C++ SFML RPG");
}

// Construcors & Destructors ==================================================================
Game::Game()
{
    this->init_window();
}

Game::~Game()
{
	delete this->window;
}

// Public Functions ===========================================================================

//	--	Update Functions ----------------------------------------------------------------------

// DELTA TIME RELATED
void Game::update_dt()
{
    /*Updates the dt variable with the time it takes to update and render one frame*/
    this->dt = this->dt_clock.restart().asSeconds();

    // TEST printing delta time
    system("cls");
    std::cout << "Delta time: " << this->dt << std::endl;
}

void Game::update_SFML_events()
{
        while (this->window->pollEvent(this->sf_event))
        {
            if (sf_event.type == sf::Event::Closed)
                this->window->close();
        }
}

void Game::update()
{
    this->update_SFML_events();
}

//	--	Render Functions ----------------------------------------------------------------------
void Game::render()
{
    this->window->clear();

    // Render items here

    this->window->display();
}

//	--	Game Loop -----------------------------------------------------------------------------
void Game::run()
{

    while (this->window->isOpen())
    {
        this->update_dt();  //  DELTA TIME RELATED
        this->update();
        this->render();
    }
}
