#include "pch.h"
#include "Button.h"

Button::Button(
	float x, float y,
	float width, float height,
	sf::Font* font, std::string text,
	sf::Color idle_color, sf::Color hover_color, sf::Color active_color)
{
	this->shape.setPosition(sf::Vector2f(x, y));
	this->shape.setSize(sf::Vector2f(width, height));

	this->font = font;
	this->text.setFont(*this->font);
	this->text.setString(text);
	this->text.setFillColor(sf::Color::White);
	this->text.setCharacterSize(20);
	this->text.setPosition(
		this->shape.getPosition().x / 2.f - this->text.getGlobalBounds().width / 2.f,
		this->shape.getPosition().y / 2.f - this->text.getGlobalBounds().height / 2.f
	);

	this->idle_color = idle_color;
	this->hover_color = hover_color;
	this->active_color = active_color;

	this->shape.setFillColor(this->idle_color);

}

Button::~Button()
{
}

void Button::update(const sf::Vector2f mouse_pos)
{
	/*
	* Update the booleans for hover and pressed
	*/

	if (this->shape.getGlobalBounds().contains(mouse_pos))
	{

	}
}

void Button::render(sf::RenderTarget* target)
{
	target->draw(this->shape);
}
