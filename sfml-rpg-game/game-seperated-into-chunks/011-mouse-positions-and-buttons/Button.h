#pragma once
class Button
{
private:
	sf::RectangleShape shape;
	sf::Font* font;
	sf::Text text;

	sf::Color idle_color;
	sf::Color hover_color;
	sf::Color active_color;

public:
	Button(
		float x, float y, 
		float width, float height, 
		sf::Font* font, std::string text,
		sf::Color idle_color, sf::Color hover_color, sf::Color active_color
	);
	~Button();

	// Functions
	void update(const sf::Vector2f mouse_pos);
	void render(sf::RenderTarget* target);
};

