// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Mouse position and Buttons
Changes made:
	Button class created
	Button included in MainMenuState

	State class:
		--	Variable for mouse positioning added
		--	Added 'virtual void update_mouse_positions();'
		NOTE	called update_mouse_positions() function from GameState and 
				MainMenuState classes in a update function

	MainMenuState class:
		--	Added init_fonts();
		--	init_fonts called from constructor
*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}