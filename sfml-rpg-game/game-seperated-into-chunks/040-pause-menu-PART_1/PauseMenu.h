#pragma once
#include "Button.h"
class PauseMenu
{
private:
	sf::RectangleShape background;
	sf::RectangleShape container;
	std::map<std::string, Button*> buttons;

public:
	// Constructors & Destructors
	PauseMenu(sf::RenderWindow& window);
	virtual ~PauseMenu();

	// Functions
	void update();
	void render(sf::RenderTarget& target);
};

