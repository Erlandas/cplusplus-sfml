#include "pch.h"
#include "Player.h"

// Initialisation blocks/functions ============================================================
void Player::init_variables()
{
}

void Player::init_components()
{
	this->create_movement_component(200.f);
}

// Construcors & Destructors ==================================================================
Player::Player(float x, float y, sf::Texture* texture)
{
	this->init_variables();
	this->init_components();
	this->create_sprite(texture);
	this->set_position(x, y);
}

Player::~Player()
{
}
