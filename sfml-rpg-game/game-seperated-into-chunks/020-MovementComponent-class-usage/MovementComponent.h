#pragma once
class MovementComponent
{
private:
	float max_velocity;

	sf::Vector2f velocity;
	sf::Vector2f acceleration;
	sf::Vector2f deceleration;

	// Initialisation blocks/functions
	

public:
	// Constructors & Destructors
	MovementComponent(float max_velocity);
	virtual ~MovementComponent();

	// Accessors
	const sf::Vector2f& get_velocity() const;

	// Functions
	void move(const float dir_x, const float dir_y);
	void update(const float& dt);
};

