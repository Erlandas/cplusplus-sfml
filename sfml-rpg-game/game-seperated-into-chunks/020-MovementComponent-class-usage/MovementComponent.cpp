#include "pch.h"
#include "MovementComponent.h"

// Initialisation blocks/functions ============================================================

// Constructors & Destructors =================================================================
MovementComponent::MovementComponent(float max_velocity)
{
	this->max_velocity = max_velocity;
}

MovementComponent::~MovementComponent()
{
}

// Accessors ==================================================================================
const sf::Vector2f& MovementComponent::get_velocity() const
{
	return this->velocity;
}

// Functions ==================================================================================
void MovementComponent::move(const float dir_x, const float dir_y)
{
	this->velocity.x = this->max_velocity * dir_x;
	this->velocity.y = this->max_velocity * dir_y;
}

void MovementComponent::update(const float& dt)
{

}
