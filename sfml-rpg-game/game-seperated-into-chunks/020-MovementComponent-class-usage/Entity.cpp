#include "pch.h"
#include "Entity.h"

// Initialisation blocks/functions ============================================================
void Entity::init_variables()
{
	this->texture = nullptr;
	this->sprite = nullptr;
	this->movement_component = nullptr;
}
// Constructors & Destructors =================================================================
Entity::Entity()
{
	this->init_variables();
	
}

Entity::~Entity()
{
	delete this->sprite;
}

// Component Functions ========================================================================
void Entity::create_sprite(sf::Texture* texture)
{
	this->texture = texture;
	this->sprite = new sf::Sprite(*this->texture);
	
}

void Entity::create_movement_component(const float max_velocity)
{
	this->movement_component = new MovementComponent(max_velocity);
}

// Functions ==================================================================================
void Entity::set_position(const float x, const float y)
{
	if (this->sprite)
	{
		this->sprite->setPosition(x, y);
	}
	
}

void Entity::move(const float& dt, const float dir_x, const float dir_y)
{
	if (this->sprite && this->movement_component)
	{
		this->movement_component->move(dir_x, dir_y); // Sets velocity
		this->sprite->move(
				this->movement_component->get_velocity() * dt // uses velocity
		);
	}
	
}

void Entity::update(const float& dt)
{

}

void Entity::render(sf::RenderTarget* target)
{
	if (this->sprite)
	{
		target->draw(*this->sprite);
	}
	
}
