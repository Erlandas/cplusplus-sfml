// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
New Movement Component class

NOTE:	MovementComponent responsible for entities movements every entity that can move
		must have MovementComponents instance

Changes made:
	--	MovementComponent class created
	--	Included in Entity class

	Entity::
	--	Movement component instance added
	--	function added 'void create_movement_component(const float max_velocity)'
	--	updated 'virtual void move(const float& dt, const float dir_x, const float dir_y);'

	Player::
	--	Move component created in init_components();	
*/


#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}