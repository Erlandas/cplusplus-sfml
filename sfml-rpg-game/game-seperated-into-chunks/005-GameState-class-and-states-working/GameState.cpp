#include "pch.h"
#include "GameState.h"

// Construcors & Destructors ==================================================================
GameState::GameState(sf::RenderWindow* window)
	: State (window)
{
}

GameState::~GameState()
{
}

// Public Functions ===========================================================================

void GameState::end_state()
{
}

//	--	Update Functions ----------------------------------------------------------------------
void GameState::update(const float& dt)
{
	std::cout << "Hello from game state!!\n";
}

//	--	Render Functions ----------------------------------------------------------------------
void GameState::render(sf::RenderTarget* target)
{
}
