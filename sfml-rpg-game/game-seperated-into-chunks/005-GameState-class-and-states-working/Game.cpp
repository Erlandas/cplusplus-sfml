#include "pch.h"
#include "Game.h"

// Initialisation blocks/functions ============================================================
void Game::init_window()
{
    // READING CONFIG FROM FILE
    /*
        Reminder working with file streams
        --  open file
        --  process file
        --  close file stream
    */
    //  --  open file
    std::ifstream ifs("Config/window.inis");
    //  --  variables that holds default values in case .ini file wont open
    std::string title{ "None" };
    sf::VideoMode window_bounds(800, 600);
    unsigned framerate_limit{ 120 };
    bool vertical_sync_enabled = false;
    //  --  process file
    if (ifs.is_open())
    {
        std::getline(ifs, title);
        ifs >> window_bounds.width >> window_bounds.height;
        ifs >> framerate_limit;
        ifs >> vertical_sync_enabled;
    }
    //  --  close file stream
    ifs.close();

	this->window = new sf::RenderWindow(window_bounds, title);
    this->window->setFramerateLimit(framerate_limit);
    this->window->setVerticalSyncEnabled(vertical_sync_enabled);
}

void Game::init_states()
{
    this->states.push(new GameState(this->window));
}

// Construcors & Destructors ==================================================================
Game::Game()
{
    this->init_window();
    this->init_states();
}

Game::~Game()
{
	delete this->window;

    while (!this->states.empty())
    {
        delete this->states.top();
        this->states.pop();
    }
}

// Public Functions ===========================================================================

//	--	Update Functions ----------------------------------------------------------------------

void Game::update_dt()
{
    /*Updates the dt variable with the time it takes to update and render one frame*/
    this->dt = this->dt_clock.restart().asSeconds();

}

void Game::update_SFML_events()
{
        while (this->window->pollEvent(this->sf_event))
        {
            if (sf_event.type == sf::Event::Closed)
                this->window->close();
        }
}

void Game::update()
{
    this->update_SFML_events();

    //  --  Update very top state
    if (!this->states.empty())
    {
        this->states.top()->update(this->dt);
    }
}

//	--	Render Functions ----------------------------------------------------------------------
void Game::render()
{
    this->window->clear();

    // Render items here
    //  --  Render very top state
    if (!this->states.empty())
    {
        this->states.top()->render();
    }

    this->window->display();
}

//	--	Game Loop -----------------------------------------------------------------------------
void Game::run()
{

    while (this->window->isOpen())
    {
        this->update_dt();
        this->update();
        this->render();
    }
}
