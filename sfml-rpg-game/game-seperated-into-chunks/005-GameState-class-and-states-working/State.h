#pragma once
class State
{
private:
	// Variables
	sf::RenderWindow* window;
	std::vector<sf::Texture*> textures;

	// Initialisation blocks/functions
public:
	// Construcors & Destructors
	State(sf::RenderWindow* window);
	virtual ~State();

	// Public Functions

	virtual void end_state() = 0;
	//	--	Update Functions
	virtual void update(const float& dt) = 0;	//	Pure virtual functions Inheritance
	//	--	Render Functions
	virtual void render(sf::RenderTarget* target = nullptr) = 0;	//	Pure virtual functions inheritance
};

