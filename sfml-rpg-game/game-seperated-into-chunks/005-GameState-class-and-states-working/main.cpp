// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Simple skeleton with impelemented GameState class that inherits its functions from state class

Following files updated with game states related code:

Game::
	Game()
	~Game()
	std::stack<State*> states;
	void init_states();
	void Game::update()
	void Game::render()

GameState:: Class Created
	GameState(sf::RenderWindow* window);
	void end_state();
	void update(const float& dt);
	void render(sf::RenderTarget* target = nullptr);
    
State::
	sf::RenderWindow* window;
	State(sf::RenderWindow* window);
	virtual void end_state() = 0;
	virtual void update(const float& dt) = 0;
	virtual void render(sf::RenderTarget* target = nullptr) = 0;
*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}