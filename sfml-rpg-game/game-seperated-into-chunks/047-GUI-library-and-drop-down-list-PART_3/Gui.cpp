#include "pch.h"
#include "Gui.h"

// <<<<<<<<<<<<<<<< BUTTON >>>>>>>>>>>>>>>>>>>

// Construcors & Destructors ==================================================================
gui::Button::Button(
	float x, float y,
	float width, float height,
	sf::Font* font, std::string text, unsigned character_size,
	sf::Color text_idle_color, sf::Color text_hover_color, sf::Color text_active_color,
	sf::Color idle_color, sf::Color hover_color, sf::Color active_color)
{
	this->button_state = BTN_IDLE;

	this->shape.setPosition(sf::Vector2f(x, y));
	this->shape.setSize(sf::Vector2f(width, height));
	this->shape.setFillColor(idle_color);

	this->font = font;
	this->text.setFont(*this->font);
	this->text.setString(text);
	this->text.setFillColor(text_idle_color);
	this->text.setCharacterSize(character_size);
	this->text.setPosition(
		this->shape.getPosition().x + (this->shape.getGlobalBounds().width / 2.f) - this->text.getGlobalBounds().width / 2.f,
		this->shape.getPosition().y + (this->shape.getGlobalBounds().height / 2.f) - this->text.getGlobalBounds().height / 2.f
	);

	this->text_idle_color = text_idle_color;
	this->text_hover_color = text_hover_color;
	this->text_active_color = text_active_color;

	this->idle_color = idle_color;
	this->hover_color = hover_color;
	this->active_color = active_color;

	

}

gui::Button::~Button()
{
}

// Accessors ==================================================================================
const bool gui::Button::is_pressed() const
{
	if (this->button_state == BTN_ACTIVE)
	{
		return true;
	}

	return false;
}

const std::string& gui::Button::get_text() const
{
	return this->text.getString();
}

// Modifiers ==================================================================================
void gui::Button::set_text(const std::string text)
{
	this->text.setString(text);
}

// Functions ==================================================================================

// Update Functions ===========================================================================
void gui::Button::update(const sf::Vector2f& mouse_pos)
{
	/* Update enum for hover and pressed*/

	// Idle
	this->button_state = BTN_IDLE;

	// Hover
	if (this->shape.getGlobalBounds().contains(mouse_pos))
	{
		this->button_state = BTN_HOVER;

		// Pressed
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			this->button_state = BTN_ACTIVE;
		}
	}

	switch (this->button_state)
	{
		case BTN_IDLE:
			this->shape.setFillColor(this->idle_color);
			this->text.setFillColor(this->text_idle_color);
			break;

		case BTN_HOVER:
			this->shape.setFillColor(this->hover_color);
			this->text.setFillColor(this->text_hover_color);
			break;

		case BTN_ACTIVE:
			this->shape.setFillColor(this->active_color);
			this->text.setFillColor(this->text_active_color);
			break;

		default:
			this->shape.setFillColor(sf::Color::Red);
			this->text.setFillColor(sf::Color::Blue);
			break;
	}
}

// Render Functions ===========================================================================
void gui::Button::render(sf::RenderTarget& target)
{
	target.draw(this->shape);
	target.draw(this->text);
}

// <<<<<<<<<<<<<<<< DROPDOWN LIST >>>>>>>>>>>>>>>>>>>

// Constructors & Destructors =================================================================
gui::DropDownList::DropDownList(
	float x, float y, float width, float height,
	sf::Font& font, std::string list[], 
	unsigned l_num_of_elements, unsigned default_index
)
	: font(font), show_list(false), key_time_max(1.f), key_time (0.f)
{
	//unsigned l_number_of_elements = sizeof(list) / sizeof(std::string);

	for (size_t i = 0; i < l_num_of_elements; i++)
	{
		this->list.push_back(new gui::Button(
				x, y + (i * height),
				width, height,
				&this->font, list[i], 12,
				sf::Color(255, 255, 255, 150), sf::Color(255, 255, 255, 255), sf::Color(20, 20, 20, 50),
				sf::Color(70, 70, 70, 200), sf::Color(150, 150, 150, 200), sf::Color(20, 20, 20, 200)
			)
		);
	}

	this->active_element = new Button(*this->list.at(default_index));


}

gui::DropDownList::~DropDownList()
{
	delete this->active_element;

	for (size_t i = 0; i < this->list.size(); i++)
	{
		delete this->list[i];
	}
}

// Accessors ==================================================================================
const bool gui::DropDownList::get_key_time()
{
	if (this->key_time >= this->key_time_max)
	{
		this->key_time = 0.f;
		return true;
	}
	return false;
}

// Functions ==================================================================================
void gui::DropDownList::update_key_time(const float& dt)
{
	if (this->key_time < this->key_time_max)
	{
		this->key_time += 10.f * dt;
	}
}

void gui::DropDownList::update(const sf::Vector2f& mouse_pos, const float& dt)
{
	this->update_key_time(dt);

	this->active_element->update(mouse_pos);

	// Show and hide the list
	if (this->active_element->is_pressed() && this->get_key_time())
	{
		if (this->show_list)
		{
			this->show_list = false;
		}	
		else
		{
			this->show_list = true;
		}		
	}

	if (this->show_list)
	{
		for (auto& i : this->list)
		{
			i->update(mouse_pos);
		}
	}

}

void gui::DropDownList::render(sf::RenderTarget& target)
{
	this->active_element->render(target);

	if (this->show_list)
	{
		for (auto& i : this->list)
		{
			i->render(target);
		}
	}
}
