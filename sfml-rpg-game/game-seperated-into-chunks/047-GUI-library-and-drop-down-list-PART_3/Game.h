#pragma once
#include "MainMenuState.h"
class Game
{
private:
	// Variables
	sf::RenderWindow* window;
	sf::Event sf_event;
	std::vector<sf::VideoMode> video_modes;
	sf::ContextSettings window_settings;
	bool fullscreen = false;

	//	--	Delta time related
	/*dt short for delta time it is going to keep track of how long it took
	for us for a game to render for up for to do one update call and one render call,
	thats a frame so one update and one render together how long did that take.
	Helps to achieve frame rate independant game.*/
	sf::Clock dt_clock;
	float dt;

	//	--	States related
	std::stack<State*> states;

	//	--	related to keybinds
	std::map<std::string, int> supported_keys;

	// Variables
	void init_variables();
	void init_window();
	void init_keys();
	void init_states();
	
public:
	// Construcors & Destructors
	Game();
	virtual ~Game();

	// Public Functions

	//	--	Reguldar Functions
	void end_application();

	//	--	Update Functions
	void update_dt();
	void update_SFML_events();
	void update();

	//	--	Render Functions
	void render();

	//	--	Game Loop CORE
	void run();
};

