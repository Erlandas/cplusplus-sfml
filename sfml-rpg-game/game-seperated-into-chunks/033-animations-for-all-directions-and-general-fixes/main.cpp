// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Animation to all directions and general fixes
Changes made:
	Player::
	--	Update() changed that flips the sprite if we walking right
	--	To flip character we have to play arountd with scale and origin
	--	Depends from direction we moving decides to flip the sprite
*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}