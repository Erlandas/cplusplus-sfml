#include "pch.h"
#include "GameState.h"
// Initialisation blocks/functions ============================================================
void GameState::init_keybinds()
{
	// Open file to read in keys
	/*
	* File format name number representing key
	*   A 0
		D 3
		W 22
		S 18
		Escape 36
	*/
	std::ifstream ifs("Config/gamestate_keybinds.ini");

	if (ifs.is_open())
	{
		std::string key = "";
		std::string key2 = "";
		while (ifs >> key >> key2)
		{
			this->keybinds[key] = this->supported_keys->at(key2);
		}

	}

}

void GameState::init_fonts()
{
	if (!this->font.loadFromFile("Fonts/Dosis-Light.ttf"))
	{
		throw("ERROR::MAINMENUSTATE::INIT_FONTS::COULD NOT LOAD FONT");
	}
}

void GameState::init_textures()
{
	if (!this->textures["PLAYER_SHEET"].loadFromFile("Resources/Images/Sprites/Player/PLAYER_SHEET.png"))
	{
		throw "!! ERROR::GAME_STATE::COULD_NOT_LOAD_PLAYER_IDLE";
	}
}

void GameState::init_pause_menu()
{
	this->pause_menu = new PauseMenu(*this->window, this->font);

	this->pause_menu->add_button("QUIT", 800.f, "Quit");
}

void GameState::init_players()
{
	this->player = new Player(400, 500, this->textures["PLAYER_SHEET"]);
}

// Construcors & Destructors ==================================================================
GameState::GameState(
	sf::RenderWindow* window, 
	std::map<std::string, 
	int>* supported_keys,
	std::stack<State*>* states
)
	: State (window, supported_keys, states)
{
	this->init_keybinds();
	this->init_fonts();
	this->init_textures();
	this->init_pause_menu();
	this->init_players();
}

GameState::~GameState()
{
	delete this->player;
	delete this->pause_menu;
}

// Public Functions ===========================================================================


//	--	Update Functions ----------------------------------------------------------------------
void GameState::update_input(const float& dt)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("CLOSE"))))
	{
		if (!this->paused)
		{
			this->pause_state();
		}
		else
		{
			this->un_pause_state();
		}
	}
}

void GameState::update_player_input(const float& dt)
{

	// Update player input
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("MOVE_LEFT"))))
	{
		this->player->move(-1.f, -0.f, dt);
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("MOVE_RIGHT"))))
	{
		this->player->move(1.f, -0.f, dt);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("MOVE_UP"))))
	{
		this->player->move( 0.f, -1.f, dt);
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("MOVE_DOWN"))))
	{
		this->player->move(0.f, 1.f, dt);
	}
}

void GameState::update_pause_menu_buttons()
{
	if (this->pause_menu->is_button_pressed("QUIT"))
	{
		this->end_state();
	}
}

void GameState::update(const float& dt)
{
	this->update_mouse_positions();
	this->update_input(dt);
	// Unpaused update
	if (!this->paused)
	{
		this->update_player_input(dt);

		this->player->update(dt);
	}
	// Paused update
	else
	{
		
		this->pause_menu->update(this->mouse_position_view);
	}
}

//	--	Render Functions ----------------------------------------------------------------------
void GameState::render(sf::RenderTarget* target)
{
	if (!target)
		target = this->window;

	this->player->render(*target);

	// Pause menu render
	if (this->paused)
	{
		this->pause_menu->render(*target);
		this->update_pause_menu_buttons();
	}
}
