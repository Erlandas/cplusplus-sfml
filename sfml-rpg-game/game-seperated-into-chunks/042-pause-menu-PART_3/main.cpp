// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Pause Menu
<<PART 3>>
Changes made:
	PauseMenu::
	--	add_button() created
	--	update() updated

	GameState::
	--	void update_pause_menu_buttons(); created

<<PART 2>>
	GameState::
	--	Changes in update function
	--	Created 'GameState::update_paused_input(const float& dt)'
	--	some renaming of methods
	--	text and fonts added to pause menu
	--	using dynamic memory for pause menu to pass fonts

<<PART 1>>
Changes made:
	State::
	--	Added boolean for pause
	--	Functions added
		+	void pause_state();
		+	void un_pause_state();

	GameState::
	--	Added conditions if it is paused in update and render methods
	--	Rendering and Updating pause menu

	NOTE: PauseMenu:: class created

*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}