#pragma once
#include "Entity.h"
class Player :
    public Entity
{
private:
    // Variables
    bool attacking;

    // Initialisation blocks/functions
    void init_variables();
    void init_components();
public:
    // Construcors & Destructors
    Player(float x, float y, sf::Texture& texture_sheet);
    virtual ~Player();

    // Public Functions

    // Update Functions
    virtual void update(const float& dt);
    // Render Functions
};

