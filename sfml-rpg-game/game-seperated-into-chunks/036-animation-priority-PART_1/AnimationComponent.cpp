#include "pch.h"
#include "AnimationComponent.h"

// Constructors & Destructors =================================================================
AnimationComponent::AnimationComponent(sf::Sprite& sprite, sf::Texture& texture_sheet)
	: sprite(sprite), texture_sheet(texture_sheet), 
	last_animation(nullptr), priority_animation(nullptr)
{

}

AnimationComponent::~AnimationComponent()
{
	for(auto & i : this->animations)
	{
		delete i.second;
	}
}
// Functions ==================================================================================

// --	Animation Related Functions ===========================================================
void AnimationComponent::add_animation(
	const std::string key, 
	float animation_timer, 
	int start_frame_x, int start_frame_y, int frames_x, int frames_y, 
	int width, int height
)
{
	this->animations[key] = new Animation(
		this->sprite, this->texture_sheet,
		animation_timer,
		start_frame_x, start_frame_y, frames_x, frames_y,
		width, height
	);
}

// --	Update Functions (play) ===============================================================
void AnimationComponent::play(const std::string key, const float& dt, const bool priority)
{

	// If there is a priority animation
	if (this->priority_animation)
	{
		if (this->priority_animation == this->animations[key])
		{
			// Reset animation, if not equal animation we trying to play
			if (this->last_animation != this->animations[key])
			{
				if (this->last_animation == nullptr)
				{
					this->last_animation = this->animations[key];
				}
				else
				{
					this->last_animation->reset();
					this->last_animation = this->animations[key];
				}

			}

			// If the priority animation iss done remove it
			if (this->animations[key]->play(dt))
			{
				this->priority_animation = nullptr;
			}
		}
	}
	// Play animation if no other priority animation is set
	else 
	{
		// if this is a priority animation set it
		if (priority)
		{
			this->priority_animation = this->animations[key];
		}
		// Reset animation, if not equal animation we trying to play
		if (this->last_animation != this->animations[key])
		{
			if (this->last_animation == nullptr)
			{
				this->last_animation = this->animations[key];
			}
			else
			{
				this->last_animation->reset();
				this->last_animation = this->animations[key];
			}
		
		}

		this->animations[key]->play(dt);
	}

	
}

void AnimationComponent::play(
	const std::string key, 
	const float& dt, 
	const float& modifier,
	const float& modifier_max, 
	const bool priority
)
{
	// If there is a priority animation
	if (this->priority_animation)
	{
		if (this->priority_animation == this->animations[key])
		{
			// Reset animation, if not equal animation we trying to play
			if (this->last_animation != this->animations[key])
			{
				if (this->last_animation == nullptr)
				{
					this->last_animation = this->animations[key];
				}
				else
				{
					this->last_animation->reset();
					this->last_animation = this->animations[key];
				}

			}


			// If the priority animation iss done remove it
			if (this->animations[key]->play(dt, abs(modifier / modifier_max)))
			{
				this->priority_animation = nullptr;
			}
			
		}
	}
	// Play animation if no other priority animation is set
	else
	{
		// if this is a priority animation set it
		if (priority)
		{
			this->priority_animation = this->animations[key];
		}
		// Reset animation, if not equal animation we trying to play
		if (this->last_animation != this->animations[key])
		{
			if (this->last_animation == nullptr)
			{
				this->last_animation = this->animations[key];
			}
			else
			{
				this->last_animation->reset();
				this->last_animation = this->animations[key];
			}

		}

		this->animations[key]->play(dt, abs(modifier / modifier_max));
	}
}

