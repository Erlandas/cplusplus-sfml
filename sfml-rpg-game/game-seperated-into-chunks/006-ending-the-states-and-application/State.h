#pragma once
class State
{
private:
	// Variables
	sf::RenderWindow* window;
	std::vector<sf::Texture*> textures;
	bool quit;

	// Initialisation blocks/functions
public:
	// Construcors & Destructors
	State(sf::RenderWindow* window);
	virtual ~State();

	// Public Functions
	const bool& get_quit() const;

	virtual void check_for_quit();
	virtual void end_state() = 0;

	//	--	Update Functions
	virtual void update_key_binds(const float& dt) = 0;
	virtual void update(const float& dt) = 0;	//	Pure virtual functions Inheritance
	//	--	Render Functions
	virtual void render(sf::RenderTarget* target = nullptr) = 0;	//	Pure virtual functions inheritance
};

