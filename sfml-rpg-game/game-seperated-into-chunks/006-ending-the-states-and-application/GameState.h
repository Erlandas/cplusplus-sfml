#pragma once
#include "State.h"

class GameState :
    public State
{
private:

public:
    // Construcors & Destructors
    GameState(sf::RenderWindow* window);
    virtual ~GameState();

    // Public Functions
    
    void end_state();
    //	--	Update Functions
    void update_key_binds(const float& dt);
    void update(const float& dt);
    //	--	Render Functions
    void render(sf::RenderTarget* target = nullptr);
};

