// main.cpp : This file contains the 'main' function. Program execution begins and ends there.

/*
Ending the states and application
Changes made:
	State class:
	--	updated check_for_quit()
	--	variable for ending state added 'bool quit = false'
	--	added 'const bool& get_quit() const'
	--	added 'virtual void update_key_binds(const float& dt) = 0;'

	GameState class:
	--	added 'void update_key_binds(const float& dt);'
	--	updated 'void end_state();'
	--	updated 'void update(const float& dt)'

	Game class:
	--	updated 'void update()'
	--	added 'void end_application()'
*/

#include "pch.h"
#include "Game.h"

int main()
{
    Game game;

    game.run();

    return 0;
}