#include "pch.h"
#include "State.h"
// Construcors & Destructors ==================================================================
State::State(sf::RenderWindow* window)
{
	this->window = window;
	this->quit = false;
}

State::~State()
{
}



// Public Functions ==========================================================================
const bool& State::get_quit() const
{
	return this->quit;
}

void State::check_for_quit()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
	{
		this->quit = true;
	}
}
