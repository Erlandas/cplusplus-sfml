#pragma once

enum button_states {
	BTN_IDLE = 0,
	BTN_HOVER = 1,
	BTN_ACTIVE = 2
};

namespace gui
{

	class Button
	{
	private:
		// Variables
		short unsigned button_state;

		sf::RectangleShape shape;
		sf::Font* font;
		sf::Text text;

		sf::Color text_idle_color;
		sf::Color text_hover_color;
		sf::Color text_active_color;

		sf::Color idle_color;
		sf::Color hover_color;
		sf::Color active_color;

	public:
		// Construcors & Destructors
		Button(
			float x, float y,
			float width, float height,
			sf::Font* font, std::string text, unsigned character_size,
			sf::Color text_idle_color, sf::Color text_hover_color, sf::Color text_active_color,
			sf::Color idle_color, sf::Color hover_color, sf::Color active_color
		);
		~Button();

		// Accessors
		const bool is_pressed() const;
		const std::string& get_text() const;

		// Modifiers
		void set_text(const std::string text);

		// Functions
		// Update Functions
		void update(const sf::Vector2f& mouse_pos);
		// Render Functions
		void render(sf::RenderTarget& target);
	};

	class DropDownList
	{
	private:
		float key_time;
		float key_time_max;
		sf::Font& font;
		gui::Button* active_element;
		std::vector<gui::Button*> list;
		bool show_list;
		

	public:
		// Constructors & Destructors
		DropDownList(
			float x, float y, float width, float height,
			sf::Font& font, std::string list[], 
			unsigned l_num_of_elements, unsigned default_index = 0
		);
		~DropDownList();

		// Accessors
		const bool get_key_time();

		// Functions
		// Update Functions
		void update_key_time(const float& dt);
		void update(
			const sf::Vector2f& mouse_pos,
			const float& dt
		);
		// Render Functions
		void render(sf::RenderTarget& target);
	};
}

