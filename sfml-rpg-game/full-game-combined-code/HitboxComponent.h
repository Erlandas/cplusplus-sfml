#pragma once
class HitboxComponent
{
private:
	// variables
	sf::Sprite& sprite;
	sf::RectangleShape hitbox;
	float offset_x;
	float offset_y;


public:
	// Constructors & Destructors
	HitboxComponent(
		sf::Sprite& sprite, 
		float offset_x, float offset_y, 
		float width, float height
	);
	virtual ~HitboxComponent();

	// Functions
	bool check_intersect(const sf::FloatRect& frect);

	void update();
	void render(sf::RenderTarget& target);
};

