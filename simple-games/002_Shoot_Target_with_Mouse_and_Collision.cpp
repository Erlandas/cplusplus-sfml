/*
*   GAME that shoots the ball to a hoop

    -   Mouse movement
    -   Mouse buttons
    -   Colission detection
*
*/

#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"

int main()
{
    // Plain window thats where we draw everything
    sf::RenderWindow window(sf::VideoMode(640, 480), "SFML works!", sf::Style::Default);

    // Important to set frame limit as it will go as much as it can if it is not set
    window.setFramerateLimit(60);

    sf::CircleShape hoop;
    int direction = 0;
    hoop.setRadius(50.f);
    hoop.setFillColor(sf::Color::Black);
    hoop.setOutlineThickness(2);
    hoop.setOutlineColor(sf::Color::Blue);
    hoop.setPosition(sf::Vector2f(0.f, 10.f));

    sf::CircleShape ball;
    bool is_shot = false;
    ball.setRadius(20.f);
    ball.setFillColor(sf::Color::Red);
    ball.setPosition(sf::Vector2f(0.f, window.getSize().y - ball.getRadius() * 3));


    // Main Loop
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }

            if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) {
                window.close();
            }
        }

        //Update

        // -  Move hoop
        if (hoop.getPosition().x <= 0) {
            direction = 1;
        }
        else if (hoop.getPosition().x + hoop.getRadius() * 2 >= window.getSize().x) {
            direction = 0;
        }
        if (direction == 0) {
            hoop.move(-5.f, 0.f);
        }
        else {
            hoop.move(5.f, 0.f);
        }
        window.clear(sf::Color::White);

        //  -   Move ball
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            is_shot = true;
        }
        if (!is_shot) {
            ball.setPosition(sf::Mouse::getPosition(window).x, ball.getPosition().y);
        }
        else {
            ball.move(0.f, -5.f);
        }
        
        //  -   Collision ball
        if (ball.getPosition().y < 0 
            || ball.getGlobalBounds().intersects(hoop.getGlobalBounds())) {

            // Reset ball
            is_shot = false;
            ball.setPosition(ball.getPosition().x, window.getSize().y - ball.getRadius() * 3);
        }




        //Draw
        window.draw(hoop);
        window.draw(ball);
        window.display();
    }

    return 0;
}