/*
* SPACE shooter game
*   --  Sprites
*   --  Collision
*   --  Controls
*   --  Randomly spawn enemies
*   --  Framerate Independant game
*   --  Delta time

*/

#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"
#include <math.h>
#include <cstdlib>
#include <vector>

/*
* VECTOR MATH:
*   LENGTH OF A VECTOR: 
           |V| = sqrt(pow(2, Vx) + pow(2, Vy))
*   NORMALIZE VECTOR: 
            U = V / |V| = (Vx, Vy) / sqrt(pow(2, Vx) + pow(2, Vy))
*/

class Bullet {
public:
    sf::Sprite shape;

    Bullet(sf::Texture* texture, sf::Vector2f pos) {
        this->shape.setTexture(*texture);
        this->shape.setScale(0.05f, 0.05f);

        this->shape.setPosition(pos);
    }

    ~Bullet() = default;

};

class Player {
public:
    sf::Sprite shape;
    sf::Texture* texture;
    int HP;
    int HPMax;

    std::vector<Bullet> bullets;
    
    Player(sf::Texture *texture) {
        this->HPMax = 10;
        this->HP = this->HPMax;

        this->texture = texture;
        this->shape.setTexture(*texture);

        this->shape.setScale(0.07f, 0.07f);
    }
    ~Player() = default;
};

class Enemy {
public:
    sf::Sprite shape;
    int HP;
    int HPMax;

    Enemy(sf::Texture* texture, sf::Vector2u window_size) {
        this->HPMax = rand() % 3 + 1;
        this->HP = this->HPMax;

        this->shape.setTexture(*texture);
        this->shape.scale(0.1f, 0.1f);
        this->shape.setPosition(
            window_size.x - this->shape.getGlobalBounds().width,
            rand() % (int)(window_size.y - this->shape.getGlobalBounds().height));
    }

    ~Enemy() = default;
};

int main()
{
    srand(time(NULL));

    // Plain window thats where we draw everything
    sf::RenderWindow window(sf::VideoMode(1400, 600), "SFML works!", sf::Style::Default);

    // Important to set frame limit as it will go as much as it can if it is not set
    window.setFramerateLimit(120);

    // Framerate Independant Stuff
    // Counting:
    /*
    * if we want to move object at 20fps speed
    * we calculate this way 20fps * delta time = result
    * then 20fps / result = multiplier
    */
    sf::Clock clock;
    float delta_time = 0.f;
    float delta_time_multiplier = 60.61f;

    // init Text
    sf::Font font;
    if (!font.loadFromFile("fonts/Arcade.ttf")) {
        std::cout << "problem loading font from 'fonts/Arcade.ttf'" << std::endl;
    }

    // init Textures
    sf::Texture player_texture;
    if (!player_texture.loadFromFile("textures/player_ship.png")) {
        std::cout << "problem loading texture from 'textures/player_texture.png'" << std::endl;
    }

    sf::Texture enemy_texture;
    if (!enemy_texture.loadFromFile("textures/enemy_ship.png")) {
        std::cout << "problem loading texture from 'textures/enemy_ship.png'" << std::endl;
    }

    sf::Texture bullet_texture;
    if (!bullet_texture.loadFromFile("textures/missile.png")) {
        std::cout << "problem loading texture from 'textures/missile.png'" << std::endl;
    }

    // UI init
    sf::Text score_text;
    score_text.setFont(font);
    score_text.setCharacterSize(30);
    score_text.setFillColor(sf::Color::White);
    score_text.setPosition(10.f, 10.f);

    sf::Text game_over_text;
    game_over_text.setFont(font);
    game_over_text.setCharacterSize(50);
    game_over_text.setFillColor(sf::Color::Red);
    game_over_text.setPosition(
        window.getSize().x / 2,
        window.getSize().y / 2);
    game_over_text.setString("GAME OVER");

    // Player init
    int score = 0;
    Player player(&player_texture);
    float shoot_timer = 20.f;
    sf::Text hp_text_player;
    hp_text_player.setFont(font);
    hp_text_player.setCharacterSize(20);
    hp_text_player.setFillColor(sf::Color::Green);

    // Enemies init
    std::vector<Enemy> enemies;
    float enemy_spawn_timer = 0.f;
    sf::Text hp_text_enemy;
    hp_text_enemy.setFont(font);
    hp_text_enemy.setCharacterSize(20);
    hp_text_enemy.setFillColor(sf::Color::Green);

    // Main Loop
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
            if (event.type == sf::Event::KeyPressed
                && event.key.code == sf::Keyboard::Escape) {
                window.close();
            }
        }

        // gives as time every frame needs to run as seconds
        // delta_time = 0.0165 seconds / frame at 60fps
        delta_time = clock.restart().asSeconds();
        std::cout << delta_time << std::endl;

        // UPDATE ====================================================================================

        if (player.HP > 0) {

            //  --  Update player
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
                player.shape.move(0.f, -10.f * delta_time * delta_time_multiplier);
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
                player.shape.move(-10.f * delta_time * delta_time_multiplier, 0.f);
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
                player.shape.move(0.f, 10.f * delta_time * delta_time_multiplier);
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
                player.shape.move(10.f * delta_time * delta_time_multiplier, 0.f);
            }

            hp_text_player.setPosition(player.shape.getPosition().x, player.shape.getPosition().y - hp_text_player.getGlobalBounds().height * 2);
            hp_text_player.setString(std::to_string(player.HP) + "/" + std::to_string(player.HPMax));


            //  --  Collision with window
            if (player.shape.getPosition().x <= 0) { // Left
                player.shape.setPosition(
                    0.f,
                    player.shape.getPosition().y
                );
            }
            if (player.shape.getPosition().x >= window.getSize().x - player.shape.getGlobalBounds().width) { // Right
                player.shape.setPosition(
                    window.getSize().x - player.shape.getGlobalBounds().width,
                    player.shape.getPosition().y
                );
            }
            if (player.shape.getPosition().y <= 0) { // Top
                player.shape.setPosition(
                    player.shape.getPosition().x,
                    0.f
                );
            }
            if (player.shape.getPosition().y >= window.getSize().y - player.shape.getGlobalBounds().height) { // Bottom
                player.shape.setPosition(
                    player.shape.getPosition().x,
                    window.getSize().y - player.shape.getGlobalBounds().height);
            }



            // Update controls
            if (shoot_timer < 20) {
                shoot_timer += 1.f * delta_time * delta_time_multiplier;
            }

            if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && shoot_timer >= 20) {
                player.bullets.push_back(Bullet(&bullet_texture, player.shape.getPosition()));
                shoot_timer = 0.f; // reset timer
            }

            // Bullets


            for (size_t i = 0; i < player.bullets.size(); i++)
            {
                // Move
                player.bullets[i].shape.move(20.f * delta_time * delta_time_multiplier, 0.f);

                //  --   out of window bounds
                if (player.bullets[i].shape.getPosition().x > window.getSize().x) {
                    player.bullets.erase(player.bullets.begin() + i);
                    break;
                }

                //  --  Enemy collision
                for (size_t k = 0; k < enemies.size(); k++)
                {
                    if (player.bullets[i].shape.getGlobalBounds().intersects(enemies[k].shape.getGlobalBounds())) {

                        if (enemies[k].HP <= 1) {
                            score += enemies[k].HPMax;
                            enemies.erase(enemies.begin() + k);
                        }
                        else {
                            enemies[k].HP--; // Enemy take damage
                        }

                        player.bullets.erase(player.bullets.begin() + i);
                        break;
                    }
                }
            }

            // Enemy
            //  --  Enemy update Spawn
            if (enemy_spawn_timer < 50) {
                enemy_spawn_timer += 1.f * delta_time * delta_time_multiplier;
            }
            if (enemy_spawn_timer >= 50) {
                enemies.push_back(Enemy(&enemy_texture, window.getSize()));
                enemy_spawn_timer = 0.f;
            }

            for (size_t i = 0; i < enemies.size(); i++)
            {
                enemies[i].shape.move(-5.f * delta_time * delta_time_multiplier, 0);

                //  Remove enemy out of bounds
                if (enemies[i].shape.getPosition().x <= 0 - enemies[i].shape.getGlobalBounds().width) {
                    enemies.erase(enemies.begin() + i);
                    break;
                }

                if (enemies[i].shape.getGlobalBounds().intersects(player.shape.getGlobalBounds())) {
                    enemies.erase(enemies.begin() + i);

                    player.HP--; // Player take damage
                    break;
                }
            }

            //UI Update
            score_text.setString("Score: " + std::to_string(score));
        }
        // DRAW ====================================================================================
        window.clear();

        //  --  Player
        window.draw(player.shape);

        //  --  Bulletsw
        for (size_t i = 0; i < player.bullets.size(); i++)
        {
            window.draw(player.bullets[i].shape);
        }
        
        //  --  Enemy
        for (size_t i = 0; i < enemies.size(); i++)
        {
            hp_text_enemy.setString(std::to_string(enemies[i].HP) + "/" + std::to_string(enemies[i].HPMax));
            hp_text_enemy.setPosition(
                enemies[i].shape.getPosition().x,
                enemies[i].shape.getPosition().y - hp_text_enemy.getGlobalBounds().height * 2);
            window.draw(hp_text_enemy);
            window.draw(enemies[i].shape);
        }

        // UIas
        window.draw(score_text);
        window.draw(hp_text_player);
        if (player.HP <= 0) {
            window.draw(game_over_text);
        }

        window.display();
    }

    return 0;
}