/*
* MINI game Cats and Dogs
* GOAL: avoid cats as a dog
* 
*   --  Mouse movement
*   --  Collision
*   --  Sprites
*   --  Textures
*   --  Health bar
*/

#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"
#include <vector>

int main()
{
    // Plain window thats where we draw everything
    sf::RenderWindow window(sf::VideoMode(1140, 580), "Mini Game!", sf::Style::Default);

    // Important to set frame limit as it will go as much as it can if it is not set
    window.setFramerateLimit(60);

    // Textures: 
    //        download some png files for a game and put beside .cpp files in project directory
    //  CAT
    //  --  Create, Load texture
    sf::Texture cat_texture;
    if (!cat_texture.loadFromFile("cat_texture.png")) {
        throw "Could not load 'cat_texture.png' !";
    }
    //  --  Create sprite and assign created texture
    sf::Sprite cat;
    cat.setTexture(cat_texture);
    //  --  Set scale for cat to make smaller
    cat.setScale(sf::Vector2f(0.2f, 0.2f));
    int cat_spawn_timer = 15;

    std::vector<sf::Sprite> cats;
    cats.push_back(sf::Sprite(cat));

    //  DOG
    sf::Texture dog_texture;
    if (!dog_texture.loadFromFile("dog_texture.png")) {
        throw "Could not load 'dog_texture.png' !";
    }
    sf::Sprite dog;
    dog.setTexture(dog_texture);
    dog.setScale(sf::Vector2f(0.03f, 0.03f));
    int health_points = 10;
    sf::RectangleShape health_bar;
    health_bar.setFillColor(sf::Color::Red);
    health_bar.setSize(sf::Vector2f((float)health_points * 20.f, 20.f));
    health_bar.setPosition(sf::Vector2f(200.f, 10.f));
    

    // Main Loop
    while (window.isOpen() && health_points > 0)
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
            if (event.type == sf::Event::KeyPressed
                && event.key.code == sf::Keyboard::Escape) {
                window.close();
            }
        }

        // UPDATE
        //  --  Update dog (player)
        dog.setPosition(
            dog.getPosition().x, 
            sf::Mouse::getPosition(window).y);

        //  --  check botom of a screen
        if (dog.getPosition().y > window.getSize().y - dog.getGlobalBounds().height) {
            dog.setPosition(
                dog.getPosition().x,
                window.getSize().y - dog.getGlobalBounds().height);
        }

        //  --  check top of a screen
        if (dog.getPosition().y < 0) {
            dog.setPosition(
                dog.getPosition().x,
                0);
        }

        //  --  Update cats (enemies)
        for (size_t i = 0; i < cats.size(); i++) {
            cats[i].move(sf::Vector2f(-5.f, 0.f));

            if (cats[i].getPosition().x < 0 - cat.getGlobalBounds().width) {
                cats.erase(cats.begin() + i);
            }
        }

        if (cat_spawn_timer < 40) {
            cat_spawn_timer++;
        }
        if (cat_spawn_timer >= 40) {
            cat.setPosition(sf::Vector2f(
                window.getSize().x, 
                rand()%window.getSize().y-cat.getGlobalBounds().height));
            cats.push_back(sf::Sprite(cat));
            cat_spawn_timer = 0;
        }// COLLISION
        for (size_t i = 0; i < cats.size(); i++)
        {
            if (dog.getGlobalBounds().intersects(cats[i].getGlobalBounds())) {
                health_points--;
                cats.erase(cats.begin() + i);
     
            }
        }

        // UI
        health_bar.setSize(sf::Vector2f((float)health_points * 20.f, 20.f));

        // DRAW
        window.clear();

        for (size_t i = 0; i < cats.size(); i++) {
            window.draw(cats[i]);
        }
        window.draw(dog);
        window.draw(health_bar);

        window.display();
    }

    return 0;
}