/*
* SIMPLE GAME that moves square
* 
*   -   Movements using keyboard events WASD
*   -   Mouse events changing colour with a left click
*   -   Colision detection
* 
*/

#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"

void update(sf::RectangleShape &square , sf::RenderWindow& window);
void draw(sf::RenderWindow &window, sf::RectangleShape& square);

int main()
{
    // Plain window thats where we draw everything
    sf::RenderWindow window(sf::VideoMode(640, 480), "Simple Square Swag!");

    // Important to set frame limit as it will go as much as it can if it is not set
    window.setFramerateLimit(60);

    sf::RectangleShape square(sf::Vector2f(100.f, 100.f));
    square.setFillColor(sf::Color::Red);
    square.setPosition(window.getSize().x / 2, window.getSize().y / 2);

    // Main Loop
    while (window.isOpen())
    {
        sf::Event event;
        // poolEvent -> getting all of the evens from the window
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
            
            if (event.KeyPressed && event.key.code == sf::Keyboard::Escape) {
                window.close();
            }
        }

        update(square, window);
        draw(window, square);
        
    }

    return 0;
}

void update(sf::RectangleShape &square, sf::RenderWindow& window) {
    // Keyboard presses
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) && 
        square.getPosition().x > 0) {
        square.move(-5.f, 0.f);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)  &&
        square.getPosition().x + square.getSize().x < window.getSize().x ) {
        square.move(5.f, 0.f);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) &&
        square.getPosition().y > 0) {
        square.move(0.f, -5.f);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) &&
        square.getPosition().y + square.getSize().y < window.getSize().y) {
        square.move(0.f, 5.f);
    }

    // Mouse 
    if (sf::Mouse::isButtonPressed(sf::Mouse::Left) ) {
        square.setFillColor(sf::Color::Blue);
    }
    else {
        square.setFillColor(sf::Color::Red);
    }


}

void draw(sf::RenderWindow &window, sf::RectangleShape& square) {
    window.clear(sf::Color::White);

    // Draw Stuff
    window.draw(square);

    window.display();
}