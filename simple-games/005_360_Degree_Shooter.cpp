/*
* Mini 360 shooting tiles game
* 
*   --  Vector maths ( count directions of bullets )
*   --  Keyboard movement
*   --  Collision
*   --  Mouse events
*/

#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"
#include <math.h>
#include <cstdlib>
#include <vector>

/*
* VECTOR MATH:
*   LENGTH OF A VECTOR: |V| = sqrt(V.x^2 + V.y^2)
*   NORMALIZE VECTOR: U = V / |V|
*/

class Bullet {
public:
    sf::CircleShape shape;
    sf::Vector2f current_velocity;
    float max_speed;

    Bullet(float radius = 5.f) 
        :current_velocity(0.f, 0.f), max_speed(15.f){
        this->shape.setRadius(radius);
        this->shape.setFillColor(sf::Color::Red);
    }
};

int main()
{
    srand(time(NULL));

    // Plain window thats where we draw everything
    sf::RenderWindow window(sf::VideoMode(940, 780), "360 Shooter", sf::Style::Default);

    // Important to set frame limit as it will go as much as it can if it is not set
    window.setFramerateLimit(60);

    // PLAYER
    sf::CircleShape player(25.f);
    player.setFillColor(sf::Color::White);

    // BULLET
    Bullet bullet_1;
    std::vector<Bullet> bullets;

    // ENEMY
    sf::RectangleShape enemy;
    enemy.setFillColor(sf::Color::Magenta);
    enemy.setSize(sf::Vector2f(50.f, 50.f));
    int enemy_spawn_counter = 20;
    std::vector<sf::RectangleShape> enemies;

    // VECTORS
    sf::Vector2f player_center;
    sf::Vector2f mouse_position_window;
    sf::Vector2f aim_direction;
    sf::Vector2f aim_direction_normalized;

    // Main Loop
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
            if (event.type == sf::Event::KeyPressed
                && event.key.code == sf::Keyboard::Escape) {
                window.close();
            }
        }

        // UPDATE

        // Vectors
        player_center = sf::Vector2f(player.getPosition().x + player.getRadius(), player.getPosition().y + player.getRadius());
        mouse_position_window = sf::Vector2f(sf::Mouse::getPosition(window));
        aim_direction = mouse_position_window - player_center;
        aim_direction_normalized = aim_direction / sqrt(pow(aim_direction.x, 2) + pow(aim_direction.y, 2));

        // std::cout << aim_direction_normalized.x << " " << aim_direction_normalized.y << "\n";

        // Player
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
            player.move(-10.f, 0.f);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
            player.move(10.f, 0.f);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
            player.move(0.f, -10.f);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
            player.move(0.f, 10.f);
        }

        // Enemies
        if (enemy_spawn_counter < 20) {
            enemy_spawn_counter++;
        }

        if (enemy_spawn_counter >= 20 && enemies.size() < 50) {
            enemy.setPosition(sf::Vector2f(
                rand()%window.getSize().x, 
                rand() % window.getSize().y));
            enemies.push_back(sf::RectangleShape(enemy));

            enemy_spawn_counter = 0;
        }
        

        // Shooting

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            bullet_1.shape.setPosition(player_center);
            bullet_1.current_velocity = aim_direction_normalized * bullet_1.max_speed;

            bullets.push_back(Bullet(bullet_1));
        }

        for (size_t i = 0; i < bullets.size(); i++)
        {
            bullets[i].shape.move(bullets[i].current_velocity);

            // Out of Window Bounds
            if (bullets[i].shape.getPosition().x < 0
                || bullets[i].shape.getPosition().x > window.getSize().x
                || bullets[i].shape.getPosition().y < 0
                || bullets[i].shape.getPosition().y > window.getSize().y) {
                bullets.erase(bullets.begin() + i);
           
            }
            else {
                // Enemy collision
                for (size_t j = 0; j < enemies.size(); j++)
                {
                    if (bullets[i].shape.getGlobalBounds().intersects(enemies[j].getGlobalBounds())) {
                        bullets.erase(bullets.begin() + i);
                        enemies.erase(enemies.begin() + j);
                        break;
                    }
                }
            }



            //std::cout << bullets.size() << std::endl;
        }

        // DRAW
        window.clear();

        for (size_t i = 0; i < enemies.size(); i++)
        {
            window.draw(enemies[i]);
        }

        window.draw(player);


        for (size_t i = 0; i < bullets.size(); i++)
        {
            window.draw(bullets[i].shape);
        }

        window.display();
    }

    return 0;
}