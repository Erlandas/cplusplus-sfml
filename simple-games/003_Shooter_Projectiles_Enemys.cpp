/*
* Simple Ball shooter game
* 
*   -   Collission detection
*   -   Random spawn of enemies
*   -   Projectiles
*   -   Mouse movement detection
*/

#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"
#include <vector>
#include <cstdlib>

int main()
{
    srand(time(NULL));

    // Plain window thats where we draw everything
    sf::RenderWindow window(
        sf::VideoMode(640, 1000), 
        "Ball Shooter!", 
        sf::Style::Default);

    // Important to set frame limit as it will go as much as it can if it is not set
    window.setFramerateLimit(60);

    // BALLS
    sf::CircleShape projectile;
    projectile.setFillColor(sf::Color::Green);
    projectile.setRadius(5.f);

    sf::RectangleShape enemy;
    enemy.setFillColor(sf::Color::Yellow);
    enemy.setSize(sf::Vector2f(50.f, 50.f));

    sf::CircleShape player;
    player.setFillColor(sf::Color::Red);
    player.setRadius(50.f);
    player.setPosition(
        window.getSize().x / 2 - player.getRadius(), 
        window.getSize().y - player.getRadius() * 2 - 10.f);
    sf::Vector2f player_center;
    int shoot_timer = 0;

    std::vector<sf::CircleShape> projectiles;
    projectiles.push_back(sf::CircleShape(projectile));

    std::vector<sf::RectangleShape> enemies;
    enemies.push_back(sf::RectangleShape(enemy));
    int enemy_spawn_timer = 0;
    
    // Main Loop
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }

            if (event.type == sf::Event::KeyPressed 
                && event.key.code == sf::Keyboard::Escape) {
                window.close();
            }
        }

        // ---- UPDATE ----

        // UPDATE PLAYER
        // Find player center
        player_center = sf::Vector2f(
            player.getPosition().x + player.getRadius(),
            player.getPosition().y + player.getRadius());

        // Set player x coordinate position by mouse position relative to window
        // We dont want to change y coordinate
        player.setPosition(
            sf::Mouse::getPosition(window).x,
            player.getPosition().y);
            /*sf::Mouse::getPosition(window).y);*/
            
        // UPDATE PROJECTILES
        // Make gaps between projectiles
        if (shoot_timer < 5) {
            shoot_timer++;
        }

        // Add projectiles with left mouse click by setting a position from middle of player
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)
            && shoot_timer >= 5) {
            projectile.setPosition(player_center);
            projectiles.push_back(sf::CircleShape(projectile));
            shoot_timer = 0;
        }

        // Add projectiles to a vector that will be shot
        for (size_t i = 0; i < projectiles.size(); i++) {
            projectiles[i].move(0.f, -10.f);

            // Remove projectile if it is out of screen bounds
            if (projectiles[i].getPosition().y <= 0) {
                projectiles.erase(projectiles.begin() + i);
            }
        }

        // UPDATE ENEMIES
        if (enemy_spawn_timer < 20) {
            enemy_spawn_timer++;
        }

        if (enemy_spawn_timer >= 20) {
            enemy.setPosition((rand() % int(window.getSize().x - enemy.getSize().x)), 0.f);

                    
            enemies.push_back(sf::RectangleShape(enemy));
            enemy_spawn_timer = 0;
        }
        for (size_t i = 0; i < enemies.size(); i++) {
            enemies[i].move(0.f, 5.f);

            if (enemies[i].getPosition().y > window.getSize().y) {
                enemies.erase(enemies.begin() + i);
            }
        }

        // COLISSION

        if (!enemies.empty() && !projectiles.empty()) {
            for (size_t i = 0; i < projectiles.size(); i++) {
                for (size_t k = 0; k < enemies.size(); k++) {
                    // check if projectiles and enemies colided
                    // if so delete both
                    // break out of a loop just to reset size of loops
                    if (projectiles[i].getGlobalBounds().intersects(enemies[k].getGlobalBounds())) {
                        projectiles.erase(projectiles.begin() + i);
                        enemies.erase(enemies.begin() + k);
                        break;
                    }
                }
            }
        }


        // DRAW
        window.clear(sf::Color::White);

        window.draw(player);

        for (size_t i = 0; i < enemies.size(); i++) {
            window.draw(enemies[i]);
        }

        for (size_t i = 0; i < projectiles.size(); i++) {
            window.draw(projectiles[i]);
        }

        window.display();
    }

    return 0;
}