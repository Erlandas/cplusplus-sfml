#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"

int main()
{
    // Plain window thats where we draw everything
    sf::RenderWindow window(sf::VideoMode(640, 480), "SFML works!", sf::Style::Default);

    // Important to set frame limit as it will go as much as it can if it is not set
    window.setFramerateLimit(60);

    // Triangle is a circle with less points if we give 3 points we have triangle
    sf::CircleShape triangle;
    triangle.setRadius(50.f);
    triangle.setPointCount(3);
    triangle.setFillColor(sf::Color::Red);

    // Line thats array of two verteces
    sf::Vertex line[] = { sf::Vertex(sf::Vector2f(100.f, 400.f)), sf::Vertex(sf::Vector2f(50.f, 100.f)) };

    // Convex shape like triangle woith more points
    sf::ConvexShape convex;
    convex.setPosition(sf::Vector2f(400.f, 50.f));
    //  -   resize it to 5 points
    convex.setPointCount(5);
    //  -   define the points
    convex.setPoint(0, sf::Vector2f(0, 0));
    convex.setPoint(1, sf::Vector2f(150, 10));
    convex.setPoint(2, sf::Vector2f(120, 90));
    convex.setPoint(3, sf::Vector2f(30, 100));
    convex.setPoint(4, sf::Vector2f(0, 50));

    // Main Loop
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
        }

        window.clear();

        //  Draw shapes
        window.draw(triangle);
        window.draw(line, 2, sf::Lines);
        window.draw(convex);

        window.display();
    }

    return 0;
}