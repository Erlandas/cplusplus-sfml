#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"

int main()
{
    // Plain window thats where we draw everything
    sf::RenderWindow window(sf::VideoMode(640, 480), "SFML works!", sf::Style::Default);
    
    // Important to set frame limit as it will go as much as it can if it is not set
    window.setFramerateLimit(60);

    // Main Loop
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }

            // Managing inputs
            //  -   First way
            //if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) {
            //    window.close();
            //}

        }

        //  -   Second way
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
            window.close();
        }

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            window.close();
        }

        window.clear();

        window.display();
    }

    return 0;
}