/*
        Using Delta Time And Acceleration
*/

#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"

int main()
{
    sf::RenderWindow window(sf::VideoMode(640, 480), "Framerate Independant!", sf::Style::Default);
    window.setFramerateLimit(200);

    // delta_time at 60fps ~ 0.016s

    sf::Clock clock;
    float delta_time;
    float multiplier = 60.f;

    sf::RectangleShape shape;
    shape.setFillColor(sf::Color::White);
    shape.setSize(sf::Vector2f(50.f, 50.f));

    sf::Vector2f current_velocity;
    sf::Vector2f direction;
    const float max_velocity = 25.f;
    const float acceleration = 1.f;
    const float drag = 0.5f;
    // Main Loop
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
            if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) {
                window.close();
            }
        }

        delta_time = clock.restart().asSeconds();

        // UPDATE =================================================

        //  --  Acceleration
        direction = sf::Vector2f(0.f, 0.f);
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
            direction.x = -1.f;

            if (current_velocity.x > -max_velocity) {
                current_velocity.x += acceleration * direction.x * delta_time * multiplier;
            }
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
            direction.x = 1.f;

            if (current_velocity.x < max_velocity) {
                current_velocity.x += acceleration * direction.x * delta_time * multiplier;
            }
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
            direction.y = -1.f;

            if (current_velocity.y > -max_velocity) {
                current_velocity.y += acceleration * direction.y * delta_time * multiplier;
            }
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
            direction.y = 1.f;

            if (current_velocity.y < max_velocity) {
                current_velocity.y += acceleration * direction.y * delta_time * multiplier;
            }
        }

        //  --  Drag
        if (current_velocity.x > 0.f) {

            current_velocity.x -= drag * delta_time * multiplier;

            if (current_velocity.x < 0.f) {
                current_velocity.x = 0.f;
            }
        }
        else if (current_velocity.x < 0.f) {
            current_velocity.x += drag * delta_time * multiplier;

            if (current_velocity.x > 0.f) {
                current_velocity.x = 0.f;
            }
        }
        else if (current_velocity.y > 0.f) {

            current_velocity.y -= drag * delta_time * multiplier;

            if (current_velocity.y < 0.f) {
                current_velocity.y = 0.f;
            }
        }
        else if (current_velocity.y < 0.f) {
            current_velocity.y += drag * delta_time * multiplier;

            if (current_velocity.y > 0.f) {
                current_velocity.y = 0.f;
            }
        }

        // Final move
        shape.move(
            current_velocity.x * delta_time * multiplier,
            current_velocity.y * delta_time * multiplier
        );

        // DRAW ===================================================
        window.clear();
        window.draw(shape);
        window.display();
        std::cout << delta_time << std::endl;
    }

    return 0;
}