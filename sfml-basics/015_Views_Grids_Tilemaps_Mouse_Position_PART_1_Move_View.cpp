#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"

int main()
{
    // init game
    float grid_size_float = 100.f;
    unsigned grid_size_unsigned = static_cast<unsigned>(grid_size_float);
    float delta_time = 0.f;
    sf::Clock delta_time_clock;
    // Those are the four vectors that we always need to track
    sf::Vector2i mouse_position_screen = sf::Mouse::getPosition(); 
    sf::Vector2i mouse_position_window;
    sf::Vector2f mouse_position_view;
    sf::Vector2u mouse_position_grid;

    // Init Window
    sf::RenderWindow window(sf::VideoMode(1000, 800), "SFML works!", sf::Style::Default);
    window.setFramerateLimit(120);
    sf::View view;
    view.setSize(1000.f, 800.f);
    // views position set from the center of the view
    view.setCenter(window.getSize().x / 2.f, window.getSize().y / 2.f);
    float view_speed = 100.f;

    // init game elements
    sf::RectangleShape shape(sf::Vector2f(grid_size_float, grid_size_float));

    // Main Loop
    while (window.isOpen())
    {
        // Update delta_time
        delta_time = delta_time_clock.restart().asSeconds();

        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
        }

        // Update
        //  --  Update input
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) // Left movement of the window not object
        {
            view.move(-view_speed * delta_time, 0.f);
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) // Right movement of the window not object
        {
            view.move(view_speed * delta_time, 0.f);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) // Up movement of the window not object
        {
            view.move(0.f ,-view_speed * delta_time);
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) // Down movement of the window not object
        {
            view.move(0.f, view_speed * delta_time);
        }

        // Render
        window.clear();
        // we draw everything using view created
        window.setView(view);

        // -- render/draw game elements
        window.draw(shape);

        // Reset view at the end of the drawing
        window.setView(window.getDefaultView());

        // -- render/draw UI

        window.display();
    }

    return 0;
}