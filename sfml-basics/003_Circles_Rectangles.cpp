#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"

int main()
{
    // Plain window thats where we draw everything
    sf::RenderWindow window(sf::VideoMode(640, 480), "SFML works!", sf::Style::Default);

    // Important to set frame limit as it will go as much as it can if it is not set
    window.setFramerateLimit(60);

    // Basic Shapes
    sf::CircleShape circle(50.f);
    circle.setPosition(sf::Vector2f(10.f, 10.f));
    circle.setFillColor(sf::Color::Blue);

    sf::RectangleShape rect(sf::Vector2f(50.f, 100.f));
    rect.setPosition(sf::Vector2f(400.f, 200.f));
    rect.setOutlineThickness(10.f);
    rect.setOutlineColor(sf::Color::Red);


    // Main Loop
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
        }

        //  Update
        circle.move(0.5f, 0.1f);
        rect.move(-0.5f, -0.1f);
        rect.rotate(0.05f);

        //  Draw

        window.clear();

        //  Draw Everything
        window.draw(circle);
        window.draw(rect);

        //  Display
        window.display();
    }

    return 0;
}