#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"
#include <sstream>
#include <vector>

int main()
{
    // init game
    float grid_size_float = 100.f;
    unsigned grid_size_unsigned = static_cast<unsigned>(grid_size_float);
    float delta_time = 0.f;
    sf::Clock delta_time_clock;
    // Those are the four vectors that we always need to track
    sf::Vector2i mouse_position_screen;
    sf::Vector2i mouse_position_window;
    sf::Vector2f mouse_position_view;
    sf::Vector2u mouse_position_grid;

    sf::Font font;
    font.loadFromFile("fonts/Arcade.ttf");
    sf::Text text;
    text.setCharacterSize(30);
    text.setFillColor(sf::Color::Blue);
    text.setFont(font);
    text.setPosition(20.f, 20.f);
    text.setString("test");

    // Init Window
    sf::RenderWindow window(sf::VideoMode(1000, 800), "SFML works!", sf::Style::Default);
    window.setFramerateLimit(120);
    sf::View view;
    view.setSize(1000.f, 800.f);
    // views position set from the center of the view
    view.setCenter(window.getSize().x / 2.f, window.getSize().y / 2.f);
    float view_speed = 200.f;

    // init game elements
    sf::RectangleShape shape(sf::Vector2f(grid_size_float, grid_size_float));

    const int map_size = 100;
    std::vector<std::vector< sf::RectangleShape>> tile_map;
    tile_map.resize(map_size, std::vector<sf::RectangleShape>());

    for (size_t x = 0; x < map_size; x++)
    {
        tile_map[x].resize(map_size, sf::RectangleShape());
        for (size_t y = 0; y < map_size; y++)
        {
            tile_map[x][y].setSize(sf::Vector2f(grid_size_float, grid_size_float));
            tile_map[x][y].setFillColor(sf::Color::White);
            tile_map[x][y].setOutlineThickness(1.f);
            tile_map[x][y].setOutlineColor(sf::Color::Black);
            tile_map[x][y].setPosition(x * grid_size_float, y * grid_size_float);
        }
    }

    // to render only what is needed
    int from_X { 0 };
    int to_X { 0 };
    int from_Y { 0 };
    int to_Y { 0 };

    sf::RectangleShape tile_selector(sf::Vector2f(grid_size_float, grid_size_float));
    tile_selector.setFillColor(sf::Color::Transparent);
    tile_selector.setOutlineThickness(1.f);
    tile_selector.setOutlineColor(sf::Color::Green);

    // Main Loop
    while (window.isOpen())
    {
        // Update delta_time
        delta_time = delta_time_clock.restart().asSeconds();

        // Update Mouse Positions
        mouse_position_screen = sf::Mouse::getPosition();
        mouse_position_window = sf::Mouse::getPosition(window);
        window.setView(view);
        mouse_position_view = window.mapPixelToCoords(mouse_position_window);
        if (mouse_position_view.x >= 0.f) {
            mouse_position_grid.x = mouse_position_view.x / static_cast<unsigned>(grid_size_float);
        }
        if (mouse_position_view.y >= 0.f) {
            mouse_position_grid.y = mouse_position_view.y / static_cast<unsigned>(grid_size_float);
        }
        window.setView(window.getDefaultView());
        
        // Update game elements
        tile_selector.setPosition(
            mouse_position_grid.x * grid_size_float, 
            mouse_position_grid.y * grid_size_float);

        // Update UI
        std::stringstream ss;
        ss << "Screen: " << mouse_position_screen.x << " " << mouse_position_screen.y << "\n"
            << "Window: " << mouse_position_window.x << " " << mouse_position_window.y << "\n"
            << "View: " << mouse_position_view.x << " " << mouse_position_view.y << "\n"
            << "Grid: " << mouse_position_grid.x << " " << mouse_position_grid.y << "\n";

        text.setString(ss.str());

        // events
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
        }

        // Update
        //  --  Update input
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) // Left movement of the window not object
        {
            view.move(-view_speed * delta_time, 0.f);
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) // Right movement of the window not object
        {
            view.move(view_speed * delta_time, 0.f);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) // Up movement of the window not object
        {
            view.move(0.f, -view_speed * delta_time);
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) // Down movement of the window not object
        {
            view.move(0.f, view_speed * delta_time);
        }

        // Render
        window.clear();
        // -- render/draw game elements
        // we draw everything using view created
        window.setView(view);

        
        window.draw(shape);

        //from_X = mouse_position_grid.x - 1;
        //to_X = mouse_position_grid.x + 2;
        //from_Y = mouse_position_grid.y - 1;
        //to_Y = mouse_position_grid.y + 2;

        from_X = view.getCenter().x / grid_size_float - 1;
        to_X = view.getCenter().x / grid_size_float + 2;
        from_Y = view.getCenter().y / grid_size_float - 1;
        to_Y = view.getCenter().y / grid_size_float + 2;
        
        if (from_X < 0) {
            from_X = 0;
        }
        else if (from_X >= map_size) {
            from_X = map_size - 1;
        }

        if (from_Y < 0) {
            from_Y = 0;
        }
        else if (from_Y >= map_size) {
            from_Y = map_size - 1;
        }

        if (to_X < 0) {
            to_X = 0;
        }
        else if (to_X >= map_size) {
            to_X = map_size - 1;
        }

        if (to_Y < 0) {
            to_Y = 0;
        }
        else if (to_Y >= map_size) {
            to_Y = map_size - 1;
        }

        for (size_t x = from_X; x < to_X; x++)
        {
            for (size_t y = from_Y; y < to_Y; y++)
            {
                window.draw(tile_map[x][y]);
            }
        }
        window.draw(tile_selector);
        // -- render/draw UI
        // Reset view at the end of the drawing
        window.setView(window.getDefaultView());

        window.draw(text);

        // Done drawing
        window.display();
    }

    return 0;
}