#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"
#include <sstream>

int main()
{
    // init game
    float grid_size_float = 100.f;
    unsigned grid_size_unsigned = static_cast<unsigned>(grid_size_float);
    float delta_time = 0.f;
    sf::Clock delta_time_clock;
    // Those are the four vectors that we always need to track
    sf::Vector2i mouse_position_screen;
    sf::Vector2i mouse_position_window;
    sf::Vector2f mouse_position_view;
    sf::Vector2u mouse_position_grid;

    sf::Font font;
    font.loadFromFile("fonts/Arcade.ttf");
    sf::Text text;
    text.setCharacterSize(30);
    text.setFillColor(sf::Color::Green);
    text.setFont(font);
    text.setPosition(20.f, 20.f);
    text.setString("test");

    // Init Window
    sf::RenderWindow window(sf::VideoMode(1000, 800), "SFML works!", sf::Style::Default);
    window.setFramerateLimit(120);
    sf::View view;
    view.setSize(1000.f, 800.f);
    // views position set from the center of the view
    view.setCenter(window.getSize().x / 2.f, window.getSize().y / 2.f);
    float view_speed = 200.f;

    // init game elements
    sf::RectangleShape shape(sf::Vector2f(grid_size_float, grid_size_float));

    // Main Loop
    while (window.isOpen())
    {
        // Update delta_time
        delta_time = delta_time_clock.restart().asSeconds();

        // Update Mouse Positions
        mouse_position_screen = sf::Mouse::getPosition();
        mouse_position_window = sf::Mouse::getPosition(window);
        window.setView(view);
        mouse_position_view = window.mapPixelToCoords(mouse_position_window);
        if (mouse_position_view.x >= 0.f) {
            mouse_position_grid.x = mouse_position_view.x / static_cast<unsigned>(grid_size_float);
        }
        if (mouse_position_view.y >= 0.f) {
            mouse_position_grid.y = mouse_position_view.y / static_cast<unsigned>(grid_size_float);
        }
        window.setView(window.getDefaultView());
        
        std::stringstream ss;
        ss << "Screen: " << mouse_position_screen.x << " " << mouse_position_screen.y << "\n"
            << "Window: " << mouse_position_window.x << " " << mouse_position_window.y << "\n"
            << "View: " << mouse_position_view.x << " " << mouse_position_view.y << "\n"
            << "Grid: " << mouse_position_grid.x << " " << mouse_position_grid.y << "\n";

        text.setString(ss.str());

        // events
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
        }

        // Update
        //  --  Update input
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) // Left movement of the window not object
        {
            view.move(-view_speed * delta_time, 0.f);
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) // Right movement of the window not object
        {
            view.move(view_speed * delta_time, 0.f);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) // Up movement of the window not object
        {
            view.move(0.f, -view_speed * delta_time);
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) // Down movement of the window not object
        {
            view.move(0.f, view_speed * delta_time);
        }

        // Render
        window.clear();
        // we draw everything using view created
        window.setView(view);

        // -- render/draw game elements
        window.draw(shape);

        // Reset view at the end of the drawing
        window.setView(window.getDefaultView());

        // -- render/draw UI

        window.draw(text);

        window.display();
    }

    return 0;
}