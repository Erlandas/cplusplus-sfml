/*
* Example of Origin and Rotation
*/

#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"

int main()
{
    sf::RenderWindow window(sf::VideoMode(800, 600), "Rotation and Origin", sf::Style::Default);
    window.setFramerateLimit(60);

    sf::RectangleShape rectangle;
    rectangle.setFillColor(sf::Color::White);
    rectangle.setSize(sf::Vector2f(100.f, 50.f));
    rectangle.setOrigin(sf::Vector2f(
        rectangle.getGlobalBounds().width / 2,
        rectangle.getGlobalBounds().height /2
    ));

    sf::CircleShape circle;
    circle.setFillColor(sf::Color::Green);
    circle.setRadius(10.f);
    circle.setOrigin(circle.getRadius(), circle.getRadius());
    circle.setPosition(sf::Vector2f(
        window.getSize().x / 2.f,
        window.getSize().y / 2.f
    ));
    

    // Main Loop
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
            if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) {
                window.close();
            }
        }

        // Update
        //  --  Move rectangle
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
            rectangle.move(-10.f, 0.f);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
            rectangle.move(10.f, 0.f);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
            rectangle.move(0.f, -10.f);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
            rectangle.move(0.f, 10.f);
        }

        if (rectangle.getGlobalBounds().intersects(circle.getGlobalBounds())) {
            circle.setFillColor(sf::Color::Red);
        }
        else {
            circle.setFillColor(sf::Color::Green);
        }


        //  --  Rotate rectangle
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
            rectangle.rotate(10.f);
        }

        // Draw
        window.clear();
        window.draw(rectangle);
        window.draw(circle);
        window.display();
    }

    return 0;
}