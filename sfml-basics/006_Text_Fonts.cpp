/*
* Fonts and Text
* download any .ttf file and install first
* keep .ttf file in project directory
*/

#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"

int main()
{
    // Plain window thats where we draw everything
    sf::RenderWindow window(sf::VideoMode(840, 480), "SFML works!", sf::Style::Default);

    // Important to set frame limit as it will go as much as it can if it is not set
    window.setFramerateLimit(60);

    // FONT
    sf::Font font;
    if (!font.loadFromFile("fonts/Wedgie Regular.ttf")) {
        std::cout << "Problem loading font from file" << std::endl;
    }
    else {
        std::cout << "No problem loading font" << std::endl;
    }

    // TEXT
    sf::Text text;
    text.setFont(font);
    text.setCharacterSize(40);
    text.setFillColor(sf::Color::Red);
    text.setString("Fonts and Text!!??!");
    text.setPosition(10.f, -10.f);

    // Main Loop
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
        }
        text.move(3.f, 0.3f);

        if (text.getPosition().x > window.getSize().x) {
            text.setPosition(-450.f, text.getPosition().y);
        }

        window.clear();
        window.draw(text);
        window.display();
    }

    return 0;
}