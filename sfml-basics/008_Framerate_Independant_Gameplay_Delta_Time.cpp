/*
        Using Delta Time
*/

#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"

int main()
{
    sf::RenderWindow window(sf::VideoMode(640, 480), "Framerate Independant!", sf::Style::Default);
    window.setFramerateLimit(60);

    sf::Clock clock;
    float delta_time;
    float multiplier = 60.f;

    sf::RectangleShape shape;
    shape.setFillColor(sf::Color::White);
    shape.setSize(sf::Vector2f(50.f, 50.f));


    // Main Loop
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
            if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) {
                window.close();
            }
        }

        delta_time = clock.restart().asSeconds();
        
        // UPDATE =================================================
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
            shape.move(-10.f * delta_time * multiplier, 0.f);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
            shape.move(10.f * delta_time * multiplier, 0.f);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
            shape.move(0.f, -10.f * delta_time * multiplier);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
            shape.move(0.f, 10.f * delta_time * multiplier);
        }

        // DRAW ===================================================
        window.clear();
        window.draw(shape);
        window.display();
        std::cout << delta_time << std::endl;
    }

    return 0;
}