#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"
#include <vector>

int main()
{
    // Window
    const unsigned WINDOW_WIDTH = 800;
    const unsigned WINDOW_HEIGHT = 600;
    sf::RenderWindow window(
        sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT),
        "SFML works!",
        sf::Style::Default);
    window.setFramerateLimit(120);

    // Delta Time
    float delta_time;
    sf::Clock delta_time_clock;

    const float grid_size = 50.f;

    // Player
    const float movement_speed = 200.f;
    sf::Vector2f velocity;
    sf::RectangleShape player;
    player.setFillColor(sf::Color::Green);
    player.setSize(sf::Vector2f(grid_size, grid_size));

    // Walls
    std::vector<sf::RectangleShape> walls;

    sf::RectangleShape wall;
    wall.setFillColor(sf::Color::Red);
    wall.setSize(sf::Vector2f(grid_size, grid_size));
    wall.setPosition(grid_size * 5, grid_size * 2);
    walls.push_back(wall);

    // Collision next position of a player
    sf::FloatRect next_position;

    sf::RectangleShape next_box;
    next_box.setSize(sf::Vector2f(grid_size, grid_size));
    next_box.setFillColor(sf::Color::Transparent);
    next_box.setOutlineColor(sf::Color::White);
    next_box.setOutlineThickness(1.f);


    // Main Loop
    while (window.isOpen())
    {
        delta_time = delta_time_clock.restart().asSeconds();

        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
            if (event.type == sf::Event::KeyPressed
                && event.key.code == sf::Keyboard::Escape) {
                window.close();
            }
        }
        // UPDATE ================================================
        // Player Movement
        velocity.y = 0.f;
        velocity.x = 0.f;

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
            velocity.y += -movement_speed * delta_time;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
            velocity.y += movement_speed * delta_time;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
            velocity.x += -movement_speed * delta_time;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
            velocity.x += movement_speed * delta_time;
        }

        player.move(velocity);

        // Collision  Regular

        for (auto& wall : walls) {
            sf::FloatRect player_bounds = player.getGlobalBounds();
            sf::FloatRect wall_bounds = wall.getGlobalBounds();

            next_position = player_bounds;
            next_position.left += velocity.x;
            next_position.top += velocity.y;

            next_box.setPosition(next_position.left, next_position.top);

            if (wall_bounds.intersects(next_position)) {
                std::cout << "COLLISION\n";
            }
        }

        

        // Collision screen
        // -- Left Collision
        if (player.getPosition().x < 0.f) { 
            player.setPosition(0.f, player.getPosition().y);
        }
        // -- Top Collision
        if (player.getPosition().y < 0.f) {
            player.setPosition(player.getPosition().x, 0.f);
        }
        // -- Right Collision
        if (player.getPosition().x + player.getGlobalBounds().width > WINDOW_WIDTH) {
            player.setPosition(WINDOW_WIDTH - player.getGlobalBounds().width, player.getPosition().y);
        }
        // -- Bottom Collision
        if (player.getPosition().y + player.getGlobalBounds().height > WINDOW_HEIGHT) {
            player.setPosition(player.getPosition().x, WINDOW_HEIGHT - player.getGlobalBounds().height);
        }


        // DRAW ==================================================
        window.clear();

        window.draw(player);

        for (auto &i : walls) {
            window.draw(i);
        }

        window.draw(next_box);

        window.display();
    }

    return 0;
}