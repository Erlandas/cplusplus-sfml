#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"

int main()
{
    // Plain window thats where we draw everything
    sf::RenderWindow window(sf::VideoMode(640, 480), "SFML works!", sf::Style::Default);

    // Important to set frame limit as it will go as much as it can if it is not set
    window.setFramerateLimit(60);

    sf::CircleShape shape(50.f);

    // Main Loop
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }  
        }

        //  Update
        shape.move(0.5f, 0.1f);

        //  Draw

        window.clear(sf::Color::Cyan);

        //  Draw Everything

        window.draw(shape);

        //  Display
        window.display();
    }

    return 0;
}