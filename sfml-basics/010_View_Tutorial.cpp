/*
* Views examples
*   --  Following player in a view
*   --  Static objects independant from main view
*/

#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"

int main()
{
    // Plain window thats where we draw everything
    sf::RenderWindow window(sf::VideoMode(800, 600), "View Tutorial!", sf::Style::Default);

    // Important to set frame limit as it will go as much as it can if it is not set
    window.setFramerateLimit(60);

    sf::View main_view;
    main_view.setSize(800, 600); // same as screen size

    sf::RectangleShape player;
    player.setSize(sf::Vector2f(50.f, 50.f));
    player.setPosition(500.f, 500.f);

    sf::RectangleShape object;
    object.setSize(sf::Vector2f(50.f, 50.f));
    object.setPosition(700.f, 600.f);
    object.setFillColor(sf::Color::Red);

    sf::RectangleShape UI_element;
    UI_element.setSize(sf::Vector2f(300.f, 20.f));
    UI_element.setPosition(10.f, 10.f);

    // Main Loop
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
            if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) {
                window.close();
            }
        }
        // Update ===================================
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
            player.move(-5.f, 0.f);
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
            player.move(5.f, 0.f);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
            player.move(0.f, -5.f);
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
            player.move(0.f, 5.f);
        }


        // center camera always around player
        main_view.setCenter(player.getPosition());


        // Draw =====================================

        window.clear();
        

        // Draw Stuff ===============================
        /*
        * when we draw elements that we want to move with a view 
        */
        window.setView(main_view);
        window.draw(player);
        window.draw(object);

        // Draw UI ==================================
        /*
        * To keep elements static on the screen we have to reset view and draw
        * Such as health points , health bar and so on
        */
        window.setView(window.getDefaultView());
        window.draw(UI_element);
        
        window.display();
    }

    return 0;
}