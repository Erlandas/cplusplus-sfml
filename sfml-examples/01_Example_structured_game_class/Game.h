#pragma once

#include <iostream>
#include <ctime>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>

class Game
{
private:
	//Variables
	sf::VideoMode video_mode;
	sf::RenderWindow* window;
	bool game_over;
	sf::Event window_event;

	void init_variables();
	void init_window();

public:

	//Constructor & Destructor
	Game();
	~Game();

	//Accessors

	//Modifiers

	//Functions
	const bool running() const;
	void poll_events();

	void update();
	void render();
};

