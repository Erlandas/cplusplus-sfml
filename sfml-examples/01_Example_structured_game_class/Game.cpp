#include "Game.h"

void Game::init_variables()
{
	this->game_over = false;
}

void Game::init_window()
{
	this->video_mode = sf::VideoMode(800, 600);
	this->window = new sf::RenderWindow(video_mode, "Swag Ballssss....", sf::Style::Close | sf::Style::Titlebar);
	this->window->setFramerateLimit(60);
}

// Constructors & Destructors
Game::Game()
{
	this->init_variables();
	this->init_window();
}

Game::~Game()
{
	delete this->window;
}


// Functions
const bool Game::running() const
{
	return this->window->isOpen();
}

void Game::poll_events()
{
	while (this->window->pollEvent(this->window_event)) 
	{
		switch (this->window_event.type) 
		{
			case sf::Event::Closed: 
			{
				this->window->close();
				break;
			}
			case sf::Event::KeyPressed:
			{
				if (this->window_event.key.code == sf::Keyboard::Escape)
				{
					this->window->close();
				}
				break;
			}
		}
	}
}

void Game::update()
{
	this->poll_events();
}

void Game::render()
{
	this->window->clear();

	this->window->display();
}

