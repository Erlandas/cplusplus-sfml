#include "Game.h"

// Functions Private ===============================================================
void Game::init_window()
{
	this->window = new sf::RenderWindow(
		sf::VideoMode(800, 600), 
		"Swaglords of Space - Game 3", 
		sf::Style::Close | sf::Style::Titlebar
	);
}

// Constructors & Destructors ======================================================
Game::Game()
{
	this->init_window();
}

Game::~Game()
{
	delete this->window;
}

// Functions Public ================================================================
void Game::run()
{
	while (this->window->isOpen())
	{
		this->update();
		this->render();
	}
	
}

void Game::update()
{
	sf::Event e;
	while (this->window->pollEvent(e)) 
	{
		if (e.Event::type == sf::Event::Closed)
		{
			this->window->close();
		}
		if (e.Event::type == sf::Event::KeyPressed
			&& e.key.code == sf::Keyboard::Escape)
		{
			this->window->close();
		}
	}
}

void Game::render()
{
	this->window->clear();

	// Draw all the stuff

	this->window->display();
}
