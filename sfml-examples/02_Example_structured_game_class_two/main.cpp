/*
Example uf structured begining project
Using main game loop in a run functions 
That is in Game.cpp
*/

#include "Game.h"

#include <iostream>



int main() {

	Game game;

	game.run();

	return 0;
}