#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

class Game
{
private:
	sf::RenderWindow* window;

	// Functions Private
	void init_window();
public:
	// Constructors & Destructors
	Game();
	virtual ~Game();

	// Functions Public
	void run();
	void update();
	void render();
};

